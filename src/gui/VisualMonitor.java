package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;

import planningSubsystem.Constants.ActorType;
import javax.swing.SwingConstants;

/**
 * Класс экрана для отображения на нём игроков
 * @author Евгений Здоровец
 */
public class VisualMonitor extends JFrame {
	private static final long serialVersionUID = 1L;
	private ArrayList<PlayerImagePanel> teammates = new ArrayList<PlayerImagePanel>();
	private ArrayList<PlayerImagePanel> enemies = new ArrayList<PlayerImagePanel>();
	private BallImagePanel ball = null;
	private PlayerImagePanel me = null;
	private static String DEFAULT_WINDOW_NAME = "Подсистема планирования действий интеллектуального агента реального времени";
	private static final int WINDOW_WIDTH = 1040;
	private static final int LOG_AREA_WIDTH = 320;
	private static final int WINDOW_HEIGHT = 640;
	private static final int OFFSET_Y = 38;
	public static final int COORDINATE_MULTIPLIER = 10;
	private static final int NEXT_BUTTON_WIDTH = 140;
	private static final int NEXT_BUTTON_HEIGHT = 20;
	private Semaphore mutex = null;
	private static Worker worker;
	private static VisualMonitor frame;
	public JTextArea logArea = new JTextArea();
	private static String filePath = "";
	/** 
	 * Метод для добавления игрока на экран
	 * @param x координата X игрока
	 * @param y координата Y игрока
	 * @param neckPosition угол поворота шеи игрока
	 */
	public void addPlayer(double x, double y, double neckPosition, ActorType actorType) {
		/*
		 * double neckPositionToDisplay = 0.0; if(side == OurTeamSide.LEFT)
		 * neckPositionToDisplay = Angle.mod180(neckPosition + 90); else if(side
		 * == OurTeamSide.RIGHT) neckPositionToDisplay =
		 * Angle.mod180(neckPosition - 90);
		 */
		PlayerImagePanel player = new PlayerImagePanel(neckPosition, actorType);
		if (actorType == ActorType.TEAMMATE) {
			teammates.add(player);
		} else if (actorType == ActorType.ENEMY) {
			enemies.add(player);
		}
		else if (actorType == ActorType.ME) {
			me = player;
		}
		player.setBackground(new Color(0, 0, 0, 0));
		player.setSize(new Dimension(30, 30));
		int newX = (int) (x * COORDINATE_MULTIPLIER) - 20;
		int newY = WINDOW_HEIGHT - ((int) (y * COORDINATE_MULTIPLIER) + 16);
		player.setLocation(newX, newY);
		this.add(player);
	}
	/** 
	 * Метод для добавления мяча на экран
	 * @param x координата X мяча
	 * @param y координата Y мяча
	 */
	public void addBall(double x, double y) {
		BallImagePanel ball = new BallImagePanel();
		this.ball = ball;
		ball.setBackground(new Color(0, 0, 0, 0));
		ball.setSize(new Dimension(20, 20));
		int newX = (int) (x * COORDINATE_MULTIPLIER) - 20;
		int newY = WINDOW_HEIGHT - ((int) (y * COORDINATE_MULTIPLIER) + 16);
		ball.setLocation(newX, newY);
		this.add(ball);
	}
	/**
	 * Метод для удаления всех игроков с экрана
	 */
	public void clearAllPlayers() {
		for (int i = 0; i < teammates.size(); i++) {
			getContentPane().remove(teammates.get(i));
		}
		teammates.clear();
		for (int i = 0; i < enemies.size(); i++) {
			getContentPane().remove(enemies.get(i));
		}
		enemies.clear();
		if(me != null)
			getContentPane().remove(me);
		me = null;
		if(ball != null)
			getContentPane().remove(ball);
		ball = null;
	}
	/**
	 * Метод для обработки нажатия на кнопку "Сброс"
	 */
	private void reset() {
		clearAllPlayers();
		worker.cancel(true);
		logArea.setText("");
		repaint();
	}
	/**
	 * Метод для обработки нажатия на кнопку "Следующий такт"
	 */
	private void processStep() {
		mutex.release(1);
		if (worker.isDone())
			logArea.setText("");
		if (worker.isCancelled() || worker.isDone()) {
			worker = new Worker(frame, mutex, filePath);
			worker.execute();
		}
	}
	/**
	 * Метод для обработки нажатия на кнопку "Загрузить из файла..."
	 */
	private void setConfigFilePath() {
		JFileChooser chooser = new JFileChooser();
	    FileNameExtensionFilter filter = new FileNameExtensionFilter(
	        "JSON input files", "json");
	    chooser.setFileFilter(filter);
	    File workingDirectory = new File(System.getProperty("user.dir"));
	    chooser.setCurrentDirectory(workingDirectory);
	    int returnVal = chooser.showOpenDialog(this);
	    if(returnVal == JFileChooser.APPROVE_OPTION) {
	    	filePath = chooser.getSelectedFile().getName();
	    	setTitle(filePath + " - " + DEFAULT_WINDOW_NAME);
	    	reset();
	    }
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					Semaphore mutex = new Semaphore(1, true);
					mutex.acquire(); // перевести мьютекс в захваченное состояние
					frame = new VisualMonitor(mutex);
					frame.setVisible(true);
					worker = new Worker(frame, mutex, filePath);
					worker.execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Конструктор
	 * @param mutex мьютекс для пошагового режима
	 */
	public VisualMonitor(Semaphore mutex) {
		File currentDir = new File(".");
		File[] matchingFiles = currentDir.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return name.endsWith("json");
		    }
		});
		if (matchingFiles.length == 0)
			System.out.println("No .json config files found in local directory!");
		else {
			for (File f : matchingFiles) {
				try {
					filePath = f.getCanonicalPath();
					setTitle(f.getName() + " - " + DEFAULT_WINDOW_NAME);
					break;
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			this.mutex = mutex;
			setResizable(false);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, WINDOW_WIDTH + LOG_AREA_WIDTH, WINDOW_HEIGHT + OFFSET_Y + 50);
			JLabel backgroundLabel = new JLabel(new ImageIcon(VisualMonitor.class.getResource("/gui/field.png")));
			backgroundLabel.setVerticalAlignment(SwingConstants.TOP);
			backgroundLabel.setHorizontalAlignment(SwingConstants.LEFT);
			setContentPane(backgroundLabel);
			getContentPane().setLayout(null);
			JButton resetButton = new JButton("Сброс");
			resetButton.setSize(NEXT_BUTTON_WIDTH, NEXT_BUTTON_HEIGHT);
			resetButton.setLocation((WINDOW_WIDTH - NEXT_BUTTON_WIDTH) / 2 - NEXT_BUTTON_WIDTH, WINDOW_HEIGHT + 20);
			resetButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					reset();
				}
			});
			getContentPane().add(resetButton);
			JButton nextButton = new JButton("Следующий такт");
			nextButton.setSize(NEXT_BUTTON_WIDTH, NEXT_BUTTON_HEIGHT);
			nextButton.setLocation((WINDOW_WIDTH - NEXT_BUTTON_WIDTH) / 2, WINDOW_HEIGHT + 20);
			nextButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					processStep();
				}
			});
			getContentPane().add(nextButton);
			JButton loadButton = new JButton("Загрузить из файла...");
			loadButton.setSize(NEXT_BUTTON_WIDTH + 30, NEXT_BUTTON_HEIGHT);
			loadButton.setLocation((WINDOW_WIDTH - NEXT_BUTTON_WIDTH) / 2 + NEXT_BUTTON_WIDTH, WINDOW_HEIGHT + 20);
			loadButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					setConfigFilePath();
				}
			});
			getContentPane().add(loadButton);
			JScrollPane sp = new JScrollPane(logArea); 
			sp.setSize(LOG_AREA_WIDTH, getHeight());
			sp.setLocation(WINDOW_WIDTH + 1, 0);
			getContentPane().add(sp);
			setLocation(0, 0);
			logArea.setFont(new Font("Arial", Font.BOLD, 14));
		}
	}
}
