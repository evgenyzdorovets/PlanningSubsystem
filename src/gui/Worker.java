package gui;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

import javax.swing.SwingWorker;

import json.JsonMapper;
import objects.Action;
import objects.Ball;
import objects.FieldObject;
import objects.Player;
import objects.ServerPlayer;
import planningSubsystem.AbsoluteCoordinate;
import planningSubsystem.ActorAct;
import planningSubsystem.Angle;
import planningSubsystem.ConcretizationAlgorithm;
import planningSubsystem.Constants;
import planningSubsystem.PlayerInfo;
import planningSubsystem.UtilityEvaluationAlgorithm;
import planningSubsystem.TestActParams;
import planningSubsystem.InputData;
import planningSubsystem.Constants.ActionTypes;
import planningSubsystem.Constants.ActorType;
import server.MyMath;
import server.ServerImitator;
import server.ServerParameters;
import utils.ActionsEnum;

/**
 * Класс произведения расчётов в отдельном потоке
 * @author Евгений Здоровец
 */
class Worker extends SwingWorker<Void, Void> {
	private VisualMonitor monitor;
	private ArrayList<PlayerInfo> teamMateInfo;
	private ArrayList<PlayerInfo> enemyInfo;
	private ServerImitator serverImitator = new ServerImitator();
	private List<Player> playerList = new ArrayList<>();
	private InputData inputData;
	private UtilityEvaluationAlgorithm predictionAlgorithm;
	private ConcretizationAlgorithm c = new ConcretizationAlgorithm();
	private Semaphore mutex = null;
	private String filePath = "";
	
	/**
	 * Конструктор
	 * @param monitor экран, которому принадлежит данный экземпляр класса расчётов
	 * @param mutex мьютекс для пошагового режима
	 * @param filePath путь к файлу для загрузки входных данных
	 */
	public Worker(VisualMonitor monitor, Semaphore mutex, String filePath) {
		this.monitor = monitor;
		this.mutex = mutex;
		this.filePath = filePath;
		predictionAlgorithm = new UtilityEvaluationAlgorithm(monitor.logArea);
	}
	/**
	 * Переопределённый метод для произведения вычислений в отдельном потоке
	 */
	@Override
	protected Void doInBackground() throws Exception {
		inputData = JsonMapper.getTestInputData(filePath);
		teamMateInfo = inputData.getTeamMateInfoIn();
		enemyInfo = inputData.getEnemyInfoIn();
		inputData.fill(serverImitator, playerList);
		ActorAct myBestAction = null;
		List<ServerPlayer> players = serverImitator.getServerPlayers();
		ArrayList<Boolean> setPlayerActions = new ArrayList<Boolean>();
		for (int i = 0; i < players.size(); i++)
			setPlayerActions.add(true);
		Player me = players.get(0).getAgentPlayer();
		Ball ball = serverImitator.getBall();
		int i = 1;
		final double kickableArea = ServerParameters.ball_size + ServerParameters.player_size + ServerParameters.kickable_margin;
		while (AbsoluteCoordinate.getDistanceBetweenCoords(me.getPosX(), me.getPosY(), ball.getPosX(), ball.getPosY()) > kickableArea) {
			publish();
			if(mutex != null)
				mutex.acquire();
			else
				Thread.sleep(700);
			TestActParams.step(teamMateInfo, enemyInfo, inputData, i, serverImitator, setPlayerActions);
			predictionAlgorithm.predictSituations(inputData);
			myBestAction = predictionAlgorithm.evaluatePredictedSituationsUtility(inputData, serverImitator.getBall());
			FieldObject destination = new FieldObject(ball.getPosX(), ball.getPosY());
			int tactsLeft = (int) ((MyMath.distance(me, destination) / ServerParameters.player_speed_max));
			int actionLength = AbsoluteCoordinate.calculatePlannedActionLength(inputData.getMyCoord(), ServerParameters.ball_speed_max, myBestAction.getActParamsAbs());
			if(actionLength > 10)
				actionLength = 10;
			if (tactsLeft > 1) {
				c.setTeamMateInfo(predictionAlgorithm.getTeamMateInfo());
				c.setEnemyInfo(predictionAlgorithm.getEnemyInfo());
				c.setMyActions(predictionAlgorithm.getMyActions());
				c.concretizeActions(inputData, actionLength + tactsLeft);
			}
			i++;
		}
		// расчёт длительности выбранного действия
		double velocityValue = myBestAction.getActParams().getDistance() * (1 - Constants.BALL_DECAY);
		int actionLength = AbsoluteCoordinate.calculatePlannedActionLength(inputData.getMyCoord(), velocityValue, myBestAction.getActParamsAbs());
		// задание планирующему игроку выбранного действия
		ActionTypes myBestActionType = ActionTypes.values()[myBestAction.getBaseActionType()];
		double x = myBestAction.getActParamsAbs().toServerFormat().getX();
		double y = myBestAction.getActParamsAbs().toServerFormat().getY();

		if (myBestActionType == ActionTypes.DRIBBLING || myBestActionType == ActionTypes.PASS || myBestActionType == ActionTypes.KICK_TO_GATE) {
			// задание планирующему игроку действия "пас"
			Action pass = me.pass(x, y, actionLength);
			me.setAction(pass);
			me.setGlobalActionType(ActionsEnum.PASS);
			setPlayerActions.set(0, false);
		}
		for (int j = 1; j <= actionLength; j++) {
			if (myBestActionType == ActionTypes.PASS) {
				int passPlayerIndex = TestActParams.getPlayerIndex(teamMateInfo, enemyInfo, myBestAction.getActorNumber());
				if (passPlayerIndex != -1) {
					// задание игроку, которому отдаётся пас, действия "открыться под пас"
					Player playerToPass = players.get(passPlayerIndex).getAgentPlayer();
					Action dash = playerToPass.intercept();
					playerToPass.setAction(dash);
					playerToPass.setGlobalActionType(ActionsEnum.INTERSEPT);
					setPlayerActions.set(passPlayerIndex, false);
				}
				else System.out.println("Warning (Worker): passPlayerIndex = -1");
			}
			publish();
			if(mutex != null)
				mutex.acquire();
			else
				Thread.sleep(700);
			TestActParams.step(teamMateInfo, enemyInfo, inputData, j, serverImitator, setPlayerActions);
			if (myBestActionType == ActionTypes.DRIBBLING /*|| myBestActionType == ActionTypes.KICK_TO_GATE*/) {
				Action dash = me.movToPos(new FieldObject(ball.getPosX(), ball.getPosY()));
				me.setAction(dash);
				me.setGlobalActionType(ActionsEnum.DASH);
			}
		}
		/*AbsCoord ballCoord2 = new AbsCoord(serverImitator.getBall().getPosX(), serverImitator.getBall().getPosY());
		System.out.println("ballCoord2: "+ ballCoord2.toSupportedFormat().getX()+" "+ ballCoord2.toSupportedFormat().getY());
		System.out.println("actionLength: " + actionLength);
		System.out.println("myBestAction: "+myBestAction.getActParamsAbs().getX()+" "+ myBestAction.getActParamsAbs().getY());*/
		return null;
	}

	/**
	 * Переопределённый метод для обновления позиций отображаемых игроков на экране
	 */
	@Override
	protected void process(final List<Void> chunks) {
		monitor.clearAllPlayers();
		AbsoluteCoordinate ballCoord = new AbsoluteCoordinate(serverImitator.getBall().getPosX(), serverImitator.getBall().getPosY());
		monitor.addBall(ballCoord.toSupportedFormat().getX(), ballCoord.toSupportedFormat().getY());
		monitor.addPlayer(inputData.getMyCoord().getX(), inputData.getMyCoord().getY(), -inputData.getMyNeckPosition(), ActorType.ME);
		for (int j = 0; j < teamMateInfo.size(); j++) {
			AbsoluteCoordinate coord = AbsoluteCoordinate.relativeToAbsoluteWithBase(inputData.getMyCoord(), inputData.getMyNeckPosition(), teamMateInfo.get(j).getRelativeCoordinates().getDirection(),
					teamMateInfo.get(j).getRelativeCoordinates().getDistance());
			monitor.addPlayer(coord.getX(), coord.getY(), Angle.mod180(90 + teamMateInfo.get(j).getRelativeCoordinates().getDirection()), ActorType.TEAMMATE);
		}
		for (int j = 0; j < enemyInfo.size(); j++) {
			AbsoluteCoordinate coord = AbsoluteCoordinate.relativeToAbsoluteWithBase(inputData.getMyCoord(), inputData.getMyNeckPosition(), enemyInfo.get(j).getRelativeCoordinates().getDirection(),
					enemyInfo.get(j).getRelativeCoordinates().getDistance());
			monitor.addPlayer(coord.getX(), coord.getY(), Angle.mod180(90 + enemyInfo.get(j).getRelativeCoordinates().getDirection()), ActorType.ENEMY);
		}
		monitor.repaint();
	}
}