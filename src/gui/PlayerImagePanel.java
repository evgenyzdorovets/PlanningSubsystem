package gui;

import java.awt.Graphics;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

import planningSubsystem.Constants.ActorType;

/**
 * Класс прорисовки игрока на экран
 * @author Евгений Здоровец
 */
public class PlayerImagePanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private double neckPosition;
	private String resourcePath;
	/**
	 * Конструктор для отображения игрока на экран
	 * @param neckPosition угол поворота шеи игрока
	 * @param actorType тип актора
	 */
	public PlayerImagePanel(double neckPosition, ActorType actorType){
		super();
		if(actorType == ActorType.TEAMMATE) {
			resourcePath = "/gui/teammate.png";
		}
		else if(actorType == ActorType.ENEMY) {
			resourcePath = "/gui/enemy.png";
		}
		else if(actorType == ActorType.ME) {
			resourcePath = "/gui/me.png";
		}
		this.neckPosition = neckPosition;
	}
	
	/**
	 * Переопределённый метод для прорисовки игрока на экран с заданным углом
	 */
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        try {
			BufferedImage image = ImageIO.read(VisualMonitor.class.getResource(resourcePath));
			double rotation = Math.toRadians (neckPosition);
			double locationX = image.getWidth() / 2;
			double locationY = image.getHeight() / 2;
			AffineTransform tx = AffineTransform.getRotateInstance(rotation, locationX, locationY);
			AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
			g.drawImage(op.filter(image, null), 0, 0, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
        
    }
}
