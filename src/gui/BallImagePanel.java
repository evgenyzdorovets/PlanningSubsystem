package gui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 * Класс прорисовки мяча на экран
 * @author Евгений Здоровец
 */
public class BallImagePanel extends JPanel {
	private static final long serialVersionUID = 1L;
	/**
	 * Конструктор для отображения мяча на экран
	 */
	public BallImagePanel(){
		super();
	}
	/**
	 * Переопределённый метод для прорисовки мяча на экран
	 */
	public void paintComponent(Graphics g){
        super.paintComponent(g);
        try {
			BufferedImage image = ImageIO.read(VisualMonitor.class.getResource("/gui/ball.png"));
			g.drawImage(image, 0, 0, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
