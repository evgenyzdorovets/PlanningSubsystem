package json;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.ObjectMapper;
import planningSubsystem.InputData;

/**
 * Класс для работы с данными в JSON-формате
 * @author Евгений Здоровец
 */
public class JsonMapper {
	/**
	 * Метод для преобразования строки в JSON-формате в объект класса входных данных InputData
	 * @param inputJson строка в JSON-формате
	 * @return объект класса входных данных InputData
	 */
	public static InputData readInputJson(String inputJson) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(Feature.ALLOW_COMMENTS, true);
        InputData data = null;
        try {
            data = mapper.readValue(inputJson, InputData.class);
        }
        catch (IOException exception) {
            exception.printStackTrace();
        }
        return data;
    }
	/**
	 * Метод для получения входных данных из файла
	 * @param fileName путь к файлу с входными данными
	 * @return экземпляр класса входных данных
	 */
	public static InputData getTestInputData(String fileName) {
		String fileContents = "";
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fileName));
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();
			    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    fileContents = sb.toString();
		}
		catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
		finally {
		    try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		InputData inputData = JsonMapper.readInputJson(fileContents);
		return inputData;
	}
}
