package planningSubsystem;

public class DistanceInfo {
	private double distance = 0.0; 
	/**
	 * Метод для получения хранимого расстояния
	 * @return хранимое расстояние
	 */
	public double getDistance(){
		return distance;
	}
	/**
	 * Конструктор по умолчанию
	 */
	public DistanceInfo(){
		
	}
	/**
	 * Конструктор
	 * @param distance расстояние
	 */
	public DistanceInfo(double distance){
		this.distance=distance;
	}
	/**
	 * Метод для изменения хранимого расстояния
	 * @param distance расстояния для присваивания
	 */
	public void setDistance(double distance){
		this.distance=distance;
	}
}
