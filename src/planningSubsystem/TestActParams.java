package planningSubsystem;

import java.util.ArrayList;
import java.util.List;

import objects.Action;
import objects.Ball;
import objects.FieldObject;
import objects.Player;
import objects.ServerPlayer;
import server.ServerImitator;
import utils.ActionsEnum;
import utils.AdditionalActionParameters;

/**
 * Класс хранения действий партнёров/оппонентов в тестовых ситуациях
 * 
 * @author Евгений Здоровец
 */
public class TestActParams {
	private ActionsEnum action;
	private int duration;
	private AbsoluteCoordinate dashCoordinate;
	private int targetPlayerNumber;
	private AdditionalActionParameters additionalParams;
	/**
	 * Метод для получения исполняемого подсистемой исполнения действия
	 * @return исполняемое подсистемой исполнения действия
	 */
	public ActionsEnum getAction() {
		return action;
	}
	/**
	 * Метод для задания исполняемого подсистемой исполнения действия
	 * @param исполняемое подсистемой исполнения действия для присваивания
	 */
	public void setAction(ActionsEnum action) {
		this.action = action;
	}
	/**
	 * Метод для получения оставшейся длительности исполняемого подсистемой исполнения действия
	 * @return оставшаяся длительность исполняемого подсистемой исполнения действия
	 */
	public int getDuration() {
		return duration;
	}
	/**
	 * Метод для задания оставшейся длительности исполняемого подсистемой исполнения действия
	 * @param оставшаяся длительность исполняемого подсистемой исполнения действия для присваивания
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}
	/**
	 * Метод для получения абсолютной координаты-назначения бега
	 * @return абсолютная координата-назначение бега
	 */
	public AbsoluteCoordinate getDashCoordinate() {
		return dashCoordinate;
	}
	/**
	 * Метод для задания абсолютной координаты-назначения бега
	 * @param абсолютная координата-назначение бега для присваивания
	 */
	public void setDashCoordinate(AbsoluteCoordinate dashCoordinate) {
		this.dashCoordinate = dashCoordinate;
	}
	/**
	 * Метод для получения номера игрока, над которым совершается действие
	 * @return номер игрока, над которым совершается действие
	 */
	public int getTargetPlayerNumber() {
		return targetPlayerNumber;
	}
	/**
	 * Метод для задания номера игрока, над которым совершается действие
	 * @param номер игрока, над которым совершается действие, для присваивания
	 */
	public void setTargetPlayerNumber(int targetPlayerNumber) {
		this.targetPlayerNumber = targetPlayerNumber;
	}
	/**
	 * Метод для получения дополнительных параметров действия
	 * @return дополнительные параметры действия
	 */
	public AdditionalActionParameters getAdditionalParams() {
		return additionalParams;
	}
	/**
	 * Метод для задания дополнительных параметров действия
	 * @param additionalParams дополнительные параметры действия для присваивания
	 */
	public void setAdditionalParams(AdditionalActionParameters additionalParams) {
		this.additionalParams = additionalParams;
	}
	/**
	 * Метод для определения индекса игрока по его номеру (для массива подсистемы исполнения)
	 * @param teamMateInfo
	 *            информация об игроках-партнёрах
	 * @param enemyInfo
	 *            информация об игроках-оппонентах
	 * @param number
	 *            номер игрока
	 * @return индекс игрока в массиве подсистемы исполнения
	 */
	public static int getPlayerIndex(ArrayList<PlayerInfo> teamMateInfo, ArrayList<PlayerInfo> enemyInfo, int number) {
		if (number == 0)
			return 0;
		else {
			for (int k = 0; k < teamMateInfo.size(); k++) {
				if (teamMateInfo.get(k).getNumber() == number)
					return k + 1;
			}
			for (int k = 0; k < enemyInfo.size(); k++) {
				if (enemyInfo.get(k).getNumber() == number)
					return k + teamMateInfo.size() + 1;
			}
		}
		return -1;
	}
	/**
	 * Метод для задания действий игроков и обновления координат от подсистемы
	 * исполнения
	 * 
	 * @param teamMateInfo
	 *            информация об игроках-партнёрах
	 * @param enemyInfo
	 *            информация об игроках-оппонентах
	 * @param inputData
	 *            входные данные для изменения
	 * @param curActionTact
	 *            номер текущего такта для присваивания
	 * @param serverImitator
	 *            класс-имитатор сервера (из подсистемы исполнения)
	 * @param setActions
	 *            массив, определяющий, задавать ли игрокам действия в теле данного метода
	 */
	public static void step(ArrayList<PlayerInfo> teamMateInfo, ArrayList<PlayerInfo> enemyInfo,
			InputData inputData, int curActionTact, ServerImitator serverImitator, ArrayList<Boolean> setActions) {
		List<ServerPlayer> serverPlayers = serverImitator.getServerPlayers();
		Player player;
			for (int i = 0; i < serverPlayers.size(); i++) {
				if (!setActions.get(i))
					continue;
				player = serverPlayers.get(i).getAgentPlayer();
				if (i == 0) {
					// планирующий игрок в течение половины времени моделирования
					// выполняет бег к мячу
					Action intercept = player.intercept();
					player.setAction(intercept);
					player.setGlobalActionType(ActionsEnum.INTERSEPT);
				} else {
					PlayerInfo info;
					if (i < teamMateInfo.size() + 1) { // задание действия для игроков-партнёров
						int teamMateIndex = i - 1;
						info = teamMateInfo.get(teamMateIndex);
					} else { // задание действия для игроков-оппонентов
						int enemyIndex = i - (teamMateInfo.size() + 1);
						info = enemyInfo.get(enemyIndex);
					}
					for (int j = 0; j < info.getActions().size(); j++) {
						TestActParams act = info.getActionAt(j);
						if (act.getDuration() > 0) {
							if (act.getAction() == ActionsEnum.DASH) {
								// задание игроку действия "бег в точку"
								Action dash = player
										.movToPos(new FieldObject(act.getDashCoordinate().toServerFormat().getX(),
												act.getDashCoordinate().toServerFormat().getY()));
								player.setAction(dash);
								player.setGlobalActionType(ActionsEnum.DASH);
							}
							else if (act.getAction() == ActionsEnum.INTERSEPT) {
								// задание игроку действия "перехват мяча"
								Action intercept = player.intercept();
								player.setAction(intercept);
								player.setGlobalActionType(ActionsEnum.INTERSEPT);
							}
							else if (act.getAction() == ActionsEnum.BLOCK_OPPONENT) {
								int playerIndex = getPlayerIndex(teamMateInfo, enemyInfo, act.getTargetPlayerNumber());
								if (playerIndex != -1) {
									// задание игроку действия "блокировка оппонента"
									Player playerToBlock = serverPlayers.get(playerIndex).getAgentPlayer();
									Action blockPlayer = player.blockOpponent(playerToBlock);
									player.setAction(blockPlayer);
									player.setGlobalActionType(ActionsEnum.BLOCK_OPPONENT);
								}
								else System.out.println("Error in TestActParams: playerIndex = -1");
							}
							else if (act.getAction() == ActionsEnum.KEEP_TO_OFFSIDE) {
								// задание игроку действия "держаться на линии последнего защитника"
								Action keepToOffside = player.keepInLineWithLastDefender(act.getAdditionalParams());
								player.setAction(keepToOffside);
								player.setGlobalActionType(ActionsEnum.KEEP_TO_OFFSIDE);
							}
							else if (act.getAction() == ActionsEnum.OUTPLAYING) {
								// задание игроку действия "открыться под пас"
								int playerIndex = getPlayerIndex(teamMateInfo, enemyInfo, act.getTargetPlayerNumber());
								if (playerIndex != -1) {
									Player playerToOutplay = serverPlayers.get(playerIndex).getAgentPlayer();
									Action outplayPlayer = player.outplayingOpponent(playerToOutplay, act.getAdditionalParams());
									player.setAction(outplayPlayer);
									player.setGlobalActionType(ActionsEnum.OUTPLAYING);
								}
								else System.out.println("Error in TestActParams: playerIndex = -1");
							}
							act.setDuration(act.getDuration() - 1);
							break;
						}
					}
				}
			}
		serverImitator.simulationStep(); // произвести такт моделирования в подсистеме исполнения
		// получить координату планирующего игрока в системе координат подсистемы исполнения
		AbsoluteCoordinate myCoord = new AbsoluteCoordinate(serverPlayers.get(0).getAgentPlayer().getPosX(),
				serverPlayers.get(0).getAgentPlayer().getPosY());
		// обновить координату планирующего игрока, переведя её в поддерживаемую систему координат
		inputData.getMyCoord().setX(myCoord.toSupportedFormat().getX());
		inputData.getMyCoord().setY(myCoord.toSupportedFormat().getY());
		for (int j = 1; j < serverPlayers.size(); j++) {
			player = serverPlayers.get(j).getAgentPlayer();
			// получить координату партнёра/оппонента
			AbsoluteCoordinate playerCoord = new AbsoluteCoordinate(player.getPosX(), player.getPosY());
			// перевести координату партнёра/оппонента в поддерживаемую систему координат
			AbsoluteCoordinate playerCoordInMyFormat = new AbsoluteCoordinate(playerCoord.toSupportedFormat().getX(),
					playerCoord.toSupportedFormat().getY());
			RelativeCoordinate relativeCoord = RelativeCoordinate.absoluteToRelative(inputData.getMyCoord(), playerCoordInMyFormat, inputData.getMyNeckPosition());
			if (j < teamMateInfo.size() + 1) {
				int teamMateIndex = j - 1;
				// задание обновлённой координаты партнёра
				teamMateInfo.get(teamMateIndex).setRelativeCoordinates(relativeCoord);
			} else {
				int enemyIndex = j - (teamMateInfo.size() + 1);
				// задание обновлённой координаты оппонента
				enemyInfo.get(enemyIndex).setRelativeCoordinates(relativeCoord);
			}
		}
		inputData.setCurrentTact(curActionTact);
		inputData.setTeamMateInfoIn(teamMateInfo);
		inputData.setEnemyInfoIn(enemyInfo);
	}
}
