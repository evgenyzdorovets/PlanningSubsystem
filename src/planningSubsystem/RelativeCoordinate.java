package planningSubsystem;

import planningSubsystem.Constants.OurTeamSide;

/**
 * Класс относительной координаты
 * @author Евгений Здоровец
 */
public class RelativeCoordinate extends DistanceInfo {
	private double direction = 0.0;
	/**
	 * Конструктор по умолчанию
	 */
	public RelativeCoordinate() {
		
	}
	/**
	 * Конструктор
	 * @param direction относительное направление
	 * @param distance относительное расстояние
	 */
	public RelativeCoordinate(double direction, double distance) {
		this.direction = direction;
		this.setDistance(distance);
	}
	/**
	 * Метод для получения направления относительной координаты
	 * @return направление координаты
	 */
	public double getDirection() {
		return direction;
	}
	/**
	 * Метод для задания направления относительной координаты
	 * @param direction относительное направление для присваивания
	 */
	public void setDirection(double direction) {
		this.direction = direction;
	}
	/**
	 * Метод для вычисления относительной координаты до ворот команды противника
	 * @param inputData входные данные
	 * @return относительная координата до ворот команды противника
	 */
	public static RelativeCoordinate calculateGateRelativeCoordinate(InputData inputData) {
		RelativeCoordinate gateCoord = new RelativeCoordinate();
		if (inputData.getOurSide() == OurTeamSide.LEFT)
		{
			gateCoord.setDirection(
					Angle.mod180(AbsoluteCoordinate.getDirectionOfCoordinate(inputData.getMyCoord(), Constants.GATE_RIGHT) - inputData.getMyNeckPosition()));
			gateCoord.setDistance(
					AbsoluteCoordinate.getDistanceBetweenCoords(inputData.getMyCoord(), Constants.GATE_RIGHT));
		}
		else if (inputData.getOurSide() == OurTeamSide.RIGHT)
		{
			gateCoord.setDirection(
					Angle.mod180(AbsoluteCoordinate.getDirectionOfCoordinate(inputData.getMyCoord(), Constants.GATE_LEFT) - inputData.getMyNeckPosition()));
			gateCoord.setDistance(
					AbsoluteCoordinate.getDistanceBetweenCoords(inputData.getMyCoord(), Constants.GATE_LEFT));
		}
		return gateCoord;
	}
	/**
	 * Метод для вычисления относительной координаты по двум абсолютным координатам
	 * @param coord1 первая абсолютная координата
	 * @param coord2 вторая абсолютная координата
	 * @param myNeckPosition угол поворота игрока
	 * @return относительная координата до ворот команды противника
	 */
	public static RelativeCoordinate absoluteToRelative(AbsoluteCoordinate coord1, AbsoluteCoordinate coord2, double myNeckPosition) {
		RelativeCoordinate coord = new RelativeCoordinate();
		coord.setDirection(
				Angle.mod180(AbsoluteCoordinate.getDirectionOfCoordinate(coord1, coord2) - myNeckPosition));
		coord.setDistance(
				AbsoluteCoordinate.getDistanceBetweenCoords(coord1, coord2));
		return coord;
	}
}
