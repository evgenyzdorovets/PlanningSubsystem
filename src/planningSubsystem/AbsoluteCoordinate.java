package planningSubsystem;

/**
 * Класс абсолютной координаты
 * @author Евгений Здоровец
 */
public class AbsoluteCoordinate {
	private double x;
	private double y;
	/**
	 * Конструктор по умолчанию
	 */
	public AbsoluteCoordinate() {

	}
	/**
	 * Конструктор
	 * @param X координата X
	 * @param Y координата Y
	 */
	public AbsoluteCoordinate(double x, double y) {
		this.x = x;
		this.y = y;
	}
	/**
	 * Метод для получения координаты X
	 * @return координата X
	 */
	public double getX() {
		return x;
	}
	/**
	 * Метод для задания координаты X
	 * @param x координата X
	 */
	public void setX(double x) {
		this.x = x;
	}
	/**
	 * Метод для получения координаты Y
	 * @return координата Y
	 */
	public double getY() {
		return y;
	}
	/**
	 * Метод для задания координаты Y
	 * @param y координата Y
	 */
	public void setY(double y) {
		this.y = y;
	}
	/**
	 * Переопределённый метод для вывода информации об объекте в виде текста
	 */
	public String toString(){
		return "X: " + x + ", Y: " + y;
	}
	/**
     * Метод для получения расстояния между двумя точками a и b
 	 * @param a - первая точка
 	 * @param b - вторая точка
 	 * @return расстояние между точками
 	 */
	public static double getDistanceBetweenCoords(AbsoluteCoordinate a, AbsoluteCoordinate b) {
		return Math.sqrt(Math.pow(a.getX() - b.getX(), 2.0) + Math.pow(a.getY() - b.getY(), 2.0));
	}
	/**
     * Метод для получения расстояния между двумя точками, заданными координатами (x1, y1) и (x2, y2)
 	 * @param x1 - координата x первой точки
 	 * @param y1 - координата y первой точки
 	 * @param x2 - координата x второй точки
 	 * @param y2 - координата y второй точки
 	 * @return расстояние между точками
 	 */
	public static double getDistanceBetweenCoords(double x1, double y1, double x2, double y2) {
		return Math.sqrt(Math.pow(x2 - x1, 2.0) + Math.pow(y2 - y1, 2.0));
	}
	/**
	 * Метод для получения угла координаты b (между осью ординат и позицией абсолютной координаты) 
	 * со смещением начала координат, задаваемым абсолютной координатой offset
	 * @param offset смещение начала координат
	 * @param b координата, для которой определяется угол
	 * @return угол между осью ординат и позицией абсолютной координаты
	 */
	public static double getDirectionOfCoordinate(AbsoluteCoordinate offset, AbsoluteCoordinate b) {
		AbsoluteCoordinate relativeToA = new AbsoluteCoordinate(b.getX() - offset.getX(), b.getY() - offset.getY());
		if (Math.abs(relativeToA.getY()) < 1e-9)
			relativeToA.y = 1e-9 * Math.signum(relativeToA.y);
		double direction = Math.atan(relativeToA.getX() / relativeToA.getY()) * 180 / Math.PI;
		if(relativeToA.getX() >= 0.0 && relativeToA.getY() >= 0.0) // I четверть
			return direction;
		else if(relativeToA.getX() < 0.0 && relativeToA.getY() >= 0.0) // II четверть
			return direction;
		else if(relativeToA.getX() < 0.0 && relativeToA.getY() < 0.0) // III четверть
			return 180 + direction;
		else  // IV четверть
			return 180 + direction;
	}
	/**
	 * Метод для перевода относительной координаты в абсолютную
	 * @param direction направление
	 * @param distance расстояние
	 * @return абсолютная координата
	 */
	public static AbsoluteCoordinate relativeToAbsolute(double direction, double distance) {
		AbsoluteCoordinate c = new AbsoluteCoordinate();
		c.x = distance * Math.sin(Math.toRadians(direction));
		c.y = distance * Math.cos(Math.toRadians(direction));
		return c;
	}
	/**
	 * Метод для перевода относительной координаты в абсолютную относительно базовой координаты
	 * @param baseCoordinate
	 * @param myNeckPosition
	 * @param direction
	 * @param distance
	 * @return абсолютная координата относительно базовой координаты
	 */
	public static AbsoluteCoordinate relativeToAbsoluteWithBase(AbsoluteCoordinate baseCoordinate,
			double myNeckPosition, double direction, double distance) {
		AbsoluteCoordinate c = new AbsoluteCoordinate();
		double absoluteDirection = Angle.mod180(myNeckPosition + direction);
		c.x = baseCoordinate.getX() + distance * Math.sin(Math.toRadians(absoluteDirection));
		c.y = baseCoordinate.getY() + distance * Math.cos(Math.toRadians(absoluteDirection));
		return c;
	}
	/**
	 * Метод для определения, достижима ли абсолютная координата из другой координаты за заданное количество тактов
	 * @param sourceCoordinate исходная координата
	 * @param destinationCoordinate координата назначения
	 * @param ticks заданное количество тактов
	 * @return true/false
	 */
	public static boolean isCoordinateReachable(AbsoluteCoordinate sourceCoordinate,
			AbsoluteCoordinate destinationCoordinate, int ticks) {
		double distance = getDistanceBetweenCoords(sourceCoordinate, destinationCoordinate);		
		return distance < Constants.PLAYER_SPEED_PER_TACT * ticks;
	}
	/**
	 * Метод для преобразования абсолютной координаты в формат для сервера (подсистемы исполнения)
	 * @param coord абсолютная координата в формате подсистемы планирования
	 * @return абсолютная координата в формате сервера (подсистемы исполнения)
	 */
	public AbsoluteCoordinate toServerFormat()
	{
		return new AbsoluteCoordinate(this.getX() - Constants.MAX_X/2, -(this.getY() - Constants.MAX_Y/2));
	}
	/**
	 * Метод для обратного преобразования абсолютной координаты в формат подсистемы планирования
	 * @param coord абсолютная координата в формате сервера (подсистемы исполнения)
	 * @return абсолютная координата в формате подсистемы планирования
	 */
	public AbsoluteCoordinate toSupportedFormat()
	{
		return new AbsoluteCoordinate(this.getX() + Constants.MAX_X/2, Constants.MAX_Y/2 - this.getY());
	}
	/**
	 * Метод для вычисления длительности планируемого действия по абсолютным координатам
	 * @param playerCoord координата игрока
	 * @param velocity скорость перемещения мяча
	 * @param actionParamCoord координата завершения действия
	 * @return длительность планируемого действия
	 */
	public static int calculatePlannedActionLength(AbsoluteCoordinate playerCoord, // координата игрока
			double ballVelocity, // скорость перемещения мяча
			AbsoluteCoordinate actionParamCoord // координата мяча
	) {
		int tacts = 0;
		double distance = 0.0, velocity = ballVelocity;
		double totalDist = AbsoluteCoordinate.getDistanceBetweenCoords(playerCoord, actionParamCoord);
		while (distance < totalDist && velocity > 0.05) {
			distance += ballVelocity;
			tacts++;
			velocity *= Constants.BALL_DECAY;
		}
		return tacts;
	}
}
