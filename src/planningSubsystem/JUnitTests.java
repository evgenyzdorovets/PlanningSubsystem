package planningSubsystem;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

import json.JsonMapper;
import objects.Player;
import planningSubsystem.Constants.ActorType;
import planningSubsystem.Constants.OurTeamSide;
import server.ServerImitator;

import org.junit.Test;

/**
 * Класс JUnit-тестов
 * @author Евгений Здоровец
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JUnitTests {
	static UtilityEvaluationAlgorithm predictionAlgorithm = new UtilityEvaluationAlgorithm(null);
	static ConcretizationAlgorithm c = new ConcretizationAlgorithm();
	private ServerImitator serverImitator = new ServerImitator();
	private List<Player> playerList = new ArrayList<>();
	@Test
	public void actionLengthMethodTest() {
		AbsoluteCoordinate playerCoord = new AbsoluteCoordinate(50.0, 70.0);
		AbsoluteCoordinate ballCoord = new AbsoluteCoordinate(50.0, 60.0);
		Assert.assertEquals(3.0, AbsoluteCoordinate.calculatePlannedActionLength(playerCoord, 4.0, ballCoord), 0.0001);
	}

	@Test
	public void predictionAlgorithmTest() {
		InputData inputData = JsonMapper.getTestInputData("situation_3x3.json");
		ArrayList<PlayerInfo> teamMateInfo = inputData.getTeamMateInfoIn();
		ArrayList<PlayerInfo> enemyInfo = inputData.getEnemyInfoIn();
		inputData.fill(serverImitator, playerList);
		ArrayList<Boolean> setPlayerActions = new ArrayList<Boolean>();
		for (int i = 0; i < serverImitator.getServerPlayers().size(); i++)
			setPlayerActions.add(true);
		int totalTacts = 5;
		for (int i = 1; i <= totalTacts; i++) {
			// изменение входных данных случайным образом
			TestActParams.step(teamMateInfo, enemyInfo, inputData, i, serverImitator, setPlayerActions);
			Assert.assertTrue(predictionAlgorithm.predictSituations(inputData));
			// Assert.assertTrue(predictionAlgorithm.checkIfUnique()); //
			// проверка на уникальность всех сформированных комбинаций
			Assert.assertNotNull(predictionAlgorithm.evaluatePredictedSituationsUtility(inputData, serverImitator.getBall()));
			if (i != totalTacts) {
				c.setTeamMateInfo(predictionAlgorithm.getTeamMateInfo());
				c.setEnemyInfo(predictionAlgorithm.getEnemyInfo());
				c.setMyActions(predictionAlgorithm.getMyActions());
				Assert.assertTrue(c.concretizeActions(inputData, totalTacts - inputData.getCurrentTact()));
			}
		}
	}
}
