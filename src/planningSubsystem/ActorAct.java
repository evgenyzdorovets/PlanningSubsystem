package planningSubsystem;

import planningSubsystem.Constants.ActorType;

/**
 * Класс действия актора
 * @author Евгений Здоровец
 */
public class ActorAct extends BaseAct implements Cloneable {
	private int type; // тип действия
	private int actorNumber = -1; // номер игрока (которому отдать пас)
	private ActorType actorType = ActorType.UNINDENTIFIED; // тип игрока, совершающего действие
	private RelativeCoordinate actParams = new RelativeCoordinate(0, -1);
	private AbsoluteCoordinate actParamsAbs = new AbsoluteCoordinate(0, -999);
	/**
	 * Конструктор
	 * @param baseAct базовое действие
	 */
	public ActorAct(BaseAct baseAct) {
		super(baseAct.actName, baseAct.baseUtility);
	}
	/**
	 * Конструктор
	 * @param baseAct базовое действие
	 * @param type тип действия
	 * @param actorType тип актора
	 * @param info информация об игроке
	 */
	public ActorAct(BaseAct baseAct, int type, ActorType actorType, PlayerInfo info) {
		super(baseAct.actName, baseAct.baseUtility);
		this.type = type;
		this.actorNumber = info.getNumber();
		this.actorType = actorType;
		this.actParams = info.getRelativeCoordinates();
	}
	/**
	 * Конструктор
	 * @param baseAct базовое действие
	 * @param type тип действия
	 * @param actorType тип актора
	 * @param actParams уточнённые параметры действия 
	 */
	public ActorAct(BaseAct baseAct, int type, ActorType actorType, RelativeCoordinate actParams) {
		super(baseAct.actName, baseAct.baseUtility);
		this.type = type;
		this.actorType = actorType;
		this.actParams = actParams;
	}
	/**
	 * Переопределённый метод для вывода информации об объекте в виде текста
	 */
	public String toString() {
		String a = new String();
		//a = a.concat("Тип базового действия: " + type);
		a = a.concat("тип: " + actName);
		a = a.concat(", базовая полезность: " + baseUtility);
		//if (actorNumber != -1)
			//a = a.concat(", номер актора: " + actorNumber);
		if (Constants.getActorMultiplier(actorType) != 0)
			a = a.concat(", команда актора: " + actorType);
		if (isConcretized()) {
			a = a.concat(",\nкоорд. X: " + Math.floor(actParamsAbs.getX() * 1000) / 1000);
			a = a.concat(", коорд. Y: " + Math.floor(actParamsAbs.getY() * 1000) / 1000);
		}
		return a;
	}
	/**
	 * Переопределённый метод для копирования объекта
	 */
	@Override
	public ActorAct clone() {
		try {
			return (ActorAct) super.clone();
		} catch (CloneNotSupportedException ex) {
			throw new InternalError();
		}
	}
	/**
	 * Метод для получения типа действия
	 * @return тип действия
	 */
	public int getBaseActionType() {
		return type;
	}
	/**
	 * Метод для получения номера актора
	 * @return номер актора
	 */
	public int getActorNumber() {
		return actorNumber;
	}
	/**
	 * Метод для получения типа актора
	 * @return тип актора (партнёр или оппонент)
	 */
	public ActorType getActorType() {
		return actorType;
	}
	/**
	 * Метод для получения параметров конкретизированного действия
	 * @return Параметры конкретизированного действия в виде относительной координаты
	 */
	public RelativeCoordinate getActParams() {
		return actParams;
	}
	/**
	 * Метод для получения параметров конкретизированного действия
	 * @return Параметры конкретизированного действия в виде абсолютной координаты
	 */
	public AbsoluteCoordinate getActParamsAbs() {
		return actParamsAbs;
	}
	/**
	 * Метод для определения, конкретизировано ли действие
	 * @return конкретизировано ли действие
	 */
	public boolean isConcretized() {
		return actParams.getDistance() != -1 || actParamsAbs.getY() != -999;
	}
	/**
	 * Метод для задания параметров действия в виде относительной координаты
	 * @param coord относительная координата для присваивания
	 */
	public void setActParams(RelativeCoordinate coord) {
		this.actParams = coord;
	}
	/**
	 * Метод для задания параметров действия в виде абсолютной координаты
	 * @param coord абсолютная координата для присваивания
	 */
	public void setActParamsAbs(AbsoluteCoordinate coord) {
		this.actParamsAbs = coord;
	}
	/**
	 * Метод для задания номера актора
	 * @param number номер для присваивания
	 */
	public void setActorNumber(int number) {
		// Метод для задания номера актора
		this.actorNumber = number;
	}
	/**
	 * Метод для задания типа действия
	 * @param type тип для присваивания
	 */
	public void setActionType(int type) {
		this.type = type;
	}
}
