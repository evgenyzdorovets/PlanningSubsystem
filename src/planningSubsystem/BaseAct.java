package planningSubsystem;
public class BaseAct {  
	protected String actName;
	protected double baseUtility;
	/**
	 * Метод для получения имени действия
	 * @return имя действия
	 */
	public String getActName()
	{
		return actName;
	}
	/**
	 * Метод для получения базовой полезности действия
	 * @return базовая полезность действия
	 */
	public double getBaseActUtility()
	{
		return baseUtility;
	}
	/**
	 * Конструктор по умолчанию
	 */
	public BaseAct(){
	}
	/**
	 * Конструктор
	 * @param name имя действия
	 * @param baseUtility базовая полезность действия
	 */
	public BaseAct(String name, double baseUtility){
		this.actName = name;
		this.baseUtility = baseUtility;
	}
}
