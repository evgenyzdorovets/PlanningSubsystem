package planningSubsystem;
/*
������ ��������� �������� ��������� ��������
� ������� ����������������� ������ ��������� �������.

�����: �������� �������
*/
//package net.sf.clipsrules.jni.examples.baseactions;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
//import net.sf.clipsrules.jni.*;
class BaseActions implements ActionListener { 
  // ������� ����� ������
  final static int MAX_ACT_COUNT = 9;
  JButton generateButton;
  JButton resetButton;
  JLabel label, label2, label3, label4,
         label5, label6, label7, label8;
  JSpinner spinner;
  JComboBox comboBoxMethod,
  comboBoxBall, //�����, ��������� �����
  comboBox_scheme, //����� �����������
  comboBox_cur_player_pos, //������� �������� ������
  comboBox_player_with_the_ball; //������� ������ � ����� (�� ��������)
  //Environment clips;//����� CLIPS
  //����������� �������, ������������ ����� ��������� �������:
  double avg_deffacts_time = 0.0, avg_reset_time = 0.0,
         avg_run_time = 0.0,      avg_eval_time = 0.0;	 
  int repeat_count = 0;//������� ���������� ����� ��������� �������
  BaseAct[] out_act = new BaseAct[MAX_ACT_COUNT];//�������� ������
  int actCount;
  SolutionTreeNode tree;
  final static String[] schemes = {
    "5-2-3",
    "4-3-3",
    "4-4-2",
  };
  final static String[] positions1 = {
    "��",
    "��",
    "��",
    "���",
    "���",
    "��",
    "���",
    "��",
    "���",
    "��",
  };
  final static String[] positions2 = {
    "��",
    "��",
    "��",
    "���",
    "���",
    "���",
    "��",
    "���",
    "���",
    "��",
  };
  final static String[] positions3 = {
    "��",
    "��",
    "���",
    "���",
    "����",
    "����",
    "��",
    "���",
    "���",
    "��",
  };
  final static int SCHEME_COUNT = 3;
  final static int POSITION_COUNT = 10;
  final static double KICK_TO_GATE_BASE_UTILITY = 0.85;
  final static double DRIBBLING_FORWARD_BASE_UTILITY = 0.8;
  final static double PASS_FORWARD_BASE_UTILITY = 0.75;
  final static double DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY = 0.6;
  final static double PASS_LEFT_OR_RIGHT_BASE_UTILITY = 0.55;
  final static double DRIBBLING_BACKWARDS_BASE_UTILITY = 0.3;
  final static double PASS_BACKWARDS_BASE_UTILITY = 0.25;
  private class SolutionTreeNode {
    // ����� ������ �������
    SolutionTreeNode leftNode;
    SolutionTreeNode rightNode;
    short type; // ��� - ���������� ������������� ������� 
    String stringData;
    BaseAct[] act = new BaseAct[MAX_ACT_COUNT];
    int curActCount;
    SolutionTreeNode root;
    BaseAct[] searchResultAct = new BaseAct[MAX_ACT_COUNT];
    SolutionTreeNode() {
      leftNode = null;
      rightNode = null;
      type = 0;
      stringData = " ";
      curActCount = 0;
      for(int i=0; i<act.length; i++)
    	  act[i] = null;
    }
    /**
     * ����� ��� ����������� �������� ��������, ���������� � ������, �� �������
     * @param index ������ �������� �������� � ��������� ������� ��������
     * @return ������� �������� � �������� ��������
     */
    BaseAct getSearchResultAct(int index) {
      return searchResultAct[index];
    }
    /**
     * ����� ��� ���������� �������� � ������
     * @param act ��� ��������
     * @param baseUtility ������� ���������� ��������
     * @return ���������� ��������
     */
    void addAct(String act, double baseUtility){
    	this.act[curActCount] = new BaseAct(act, baseUtility);
    	curActCount++;
    }
    /**
     * ����� ��� ��������� ���������� �������� � ���� ������
     * @return ���������� ��������
     */
    int getActCount(){
    	return curActCount;
    }
    /**
     * ����� ��� ���������� ������ �������
     */
    void buildTree() {
      SolutionTreeNode prev1 = null, prev2 = null, prev3 = null;
      SolutionTreeNode schemesCheck1 = null,
        schemesCheck2 = null;
      SolutionTreeNode curPosCheck1 = null,
        curPosCheck2 = null;
      SolutionTreeNode playerWithBallPosCheck = null;

      root = new SolutionTreeNode();
      root.type = 1;

      for(int i = 0; i < SCHEME_COUNT; i++) {
        schemesCheck1 = new SolutionTreeNode();
        schemesCheck1.stringData = schemes[i];
        schemesCheck1.type = 2;
        if(i == 0) root.leftNode = schemesCheck1;
        else prev1.rightNode = schemesCheck1;
        prev1 = schemesCheck1;
        schemesCheck2 = new SolutionTreeNode();
        schemesCheck2.stringData = schemes[i];
        schemesCheck2.type = 2;
        if(i == 0) root.rightNode = schemesCheck2;
        else prev2.rightNode = schemesCheck2;
        prev2 = schemesCheck2;
      }

      for(int i = 0; i < SCHEME_COUNT; i++) {
        for(int j = 0; j < POSITION_COUNT; j++) {
          // ������� �������� ������, ���� ��� "� ����"
          curPosCheck1 = new SolutionTreeNode(); 
          if(i == 0) curPosCheck1.stringData = positions1[j];
          else if(i == 1) curPosCheck1.stringData = positions2[j];
          else if(i == 2) curPosCheck1.stringData = positions3[j];
          curPosCheck1.type = 3;
          if(j == 0) {
            if(i == 0) root.leftNode.leftNode = curPosCheck1;
            if(i == 1) root.leftNode.rightNode.leftNode = curPosCheck1;
            if(i == 2) root.leftNode.rightNode.rightNode.leftNode = curPosCheck1;
          } else prev1.rightNode = curPosCheck1;
          prev1 = curPosCheck1;
          // ���������� ����������� � ����� 
          String str = curPosCheck1.stringData;
          for(int it=0; it<MAX_ACT_COUNT; it++)
        	  curPosCheck1.act[it] = new BaseAct("���",0); //�� ���������
          if(i == 0) //������ ����� �����������
          {
            if(str == positions1[0]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
              curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
              curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
              curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
              curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
              curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);              
              curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);              
            }
            else if(str == positions1[1]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
              curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
              curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
              curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
              curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
              curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
              curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
              curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions1[2]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions1[3]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            
                curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);             
                curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
                curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions1[4]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);           
                curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
                curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
                curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions1[5]) {
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	           
                curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
                curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
                curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions1[6] || str == positions1[7] 
            || str == positions1[8]) {
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions1[9]) {
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
          } else if(i == 1) //������ ����� �����������
          {
            if(str == positions2[0]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions2[1]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions2[2]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions2[3]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions2[4]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions2[5]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);   
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);         	
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions2[6]) {
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions2[7] || str == positions2[8]) {
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions2[9]) {
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
          } else if(i == 2) //������ ����� �����������
          {
            if(str == positions3[0]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
              curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
              curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
              curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);              
              curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
              curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
              curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions3[1]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions3[2]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions3[4] || str == positions3[5]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions3[3]) {
            	curPosCheck1.addAct("���� �� �������",KICK_TO_GATE_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions3[6]) {
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions3[7] || str == positions3[8]) {
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
                curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);                
                curPosCheck1.addAct("��� ������",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
            else if(str == positions3[9]) {
            	curPosCheck1.addAct("������� ������",DRIBBLING_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("��� ������",PASS_FORWARD_BASE_UTILITY);
            	curPosCheck1.addAct("������� �����",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);
            	curPosCheck1.addAct("������� ������",DRIBBLING_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("��� �����",PASS_LEFT_OR_RIGHT_BASE_UTILITY);            	
            	curPosCheck1.addAct("������� �����",DRIBBLING_BACKWARDS_BASE_UTILITY);
            	curPosCheck1.addAct("��� �����",PASS_BACKWARDS_BASE_UTILITY);
            }
          }
          //----������� �������� ������, ���� ��� "�� � ����"----
          curPosCheck2 = new SolutionTreeNode(); 
          if(i == 0) curPosCheck2.stringData = positions1[j];
          else if(i == 1) curPosCheck2.stringData = positions2[j];
          else if(i == 2) curPosCheck2.stringData = positions3[j];
          curPosCheck2.type = 4;
          if(j == 0) {
            if(i == 0) root.rightNode.leftNode = curPosCheck2;
            if(i == 1) root.rightNode.rightNode.leftNode = curPosCheck2;
            if(i == 2) root.rightNode.rightNode.rightNode.leftNode = curPosCheck2;
          } else prev2.rightNode = curPosCheck2;
          prev2 = curPosCheck2;

          //----������� ������ � �����, ���� ��� "�� � ����"----
          for(int k = 0; k < POSITION_COUNT; k++) {
            playerWithBallPosCheck = new SolutionTreeNode();
            if(i == 0) playerWithBallPosCheck.stringData = positions1[k];
            else if(i == 1) playerWithBallPosCheck.stringData = positions2[k];
            else if(i == 2) playerWithBallPosCheck.stringData = positions3[k];
            playerWithBallPosCheck.type = 5;
            if(k == 0) curPosCheck2.leftNode = playerWithBallPosCheck;
            else prev3.rightNode = playerWithBallPosCheck;
            prev3 = playerWithBallPosCheck;
            //----���������� ����������� � �����----
            boolean change = false;
            for(int it=0; it<MAX_ACT_COUNT; it++)
            	playerWithBallPosCheck.act[it] = new BaseAct("���",0); //�� ���������
            playerWithBallPosCheck.act[0] = new BaseAct("������� ����",0.6); //�� ���������
            String s = playerWithBallPosCheck.stringData;
            if(i == 0) //������ ����� �����������
            {
              if(curPosCheck2.stringData == positions1[0])
                if(s == "��" || s == "���") change = true;
              if(curPosCheck2.stringData == positions1[1])
                if(s == "��" || s == "��" || s == "���" || s == "���") change = true;
              if(curPosCheck2.stringData == positions1[2])
                if(s == "��" || s == "���") change = true;
              if(curPosCheck2.stringData == positions1[3])
                if(s == "��" || s == "��" || s == "���" || s == "��" 
                || s == "���" || s == "��") change = true;
              if(curPosCheck2.stringData == positions1[4])
                if(s == "��" || s == "��" || s == "���" || s == "��" 
                || s == "���" || s == "��") change = true;
              if(curPosCheck2.stringData == positions1[5])
                if(s == "���" || s == "���") change = true;
              if(curPosCheck2.stringData == positions1[6])
                if(s == "���" || s == "���" || s == "��") change = true;
              if(curPosCheck2.stringData == positions1[7])
                if(s == "���" || s == "���" || s == "���" || s == "���") change = true;
              if(curPosCheck2.stringData == positions1[8])
                if(s == "���" || s == "��" || s == "��") change = true;
              if(curPosCheck2.stringData == positions1[9])
                if(s == "���" || s == "���") change = true;
            } else if(i == 1) //������ ����� �����������
            {
              if(curPosCheck2.stringData == positions2[0])
                if(s == "��" || s == "���" || s == "���") change = true;
              if(curPosCheck2.stringData == positions2[1])
                if(s == "��" || s == "��" || s == "���" 
                || s == "���" || s == "���") change = true;
              if(curPosCheck2.stringData == positions2[2])
                if(s == "��" || s == "���" || s == "���") change = true;
              if(curPosCheck2.stringData == positions2[3])
                if(s == "��" || s == "��" || s == "���" 
                || s == "��" || s == "���") change = true;
              if(curPosCheck2.stringData == positions2[4])
                if(s == "��" || s == "��" || s == "��" || s == "���" 
                || s == "���" || s == "���" || s == "���") change = true;
              if(curPosCheck2.stringData == positions2[5])
                if(s == "��" || s == "��" || s == "���" 
                || s == "��" || s == "���") change = true;
              if(curPosCheck2.stringData == positions2[6])
                if(s == "���" || s == "���") change = true;
              if(curPosCheck2.stringData == positions2[7])
                if(s == "���" || s == "���" 
                || s == "��" || s == "���") change = true;
              if(curPosCheck2.stringData == positions2[8])
                if(s == "���" || s == "���" 
                || s == "��" || s == "���") change = true;
              if(curPosCheck2.stringData == positions2[9])
                if(s == "���" || s == "���") change = true;
            } else if(i == 2) //������ ����� �����������
            {
              if(curPosCheck2.stringData == positions3[0])
                if(s == "��" || s == "���" 
                || s == "����" || s == "����") change = true;
              if(curPosCheck2.stringData == positions3[1])
                if(s == "��" || s == "���" 
                || s == "����" || s == "����") change = true;
              if(curPosCheck2.stringData == positions3[2])
                if(s == "��" || s == "����" 
                || s == "���" || s == "��") change = true;
              if(curPosCheck2.stringData == positions3[3])
                if(s == "��" || s == "��" || s == "����" || s == "���" 
                || s == "���" || s == "��" || s == "���") change = true;
              if(curPosCheck2.stringData == positions3[4])
                if(s == "��" || s == "��" || s == "����" || s == "���" 
                || s == "���" || s == "��" || s == "���") change = true;
              if(curPosCheck2.stringData == positions3[5])
                if(s == "��" || s == "����" 
                || s == "���" || s == "��") change = true;
              if(curPosCheck2.stringData == positions3[6])
                if(s == "���" || s == "����" || s == "���") change = true;
              if(curPosCheck2.stringData == positions3[7])
                if(s == "��" || s == "���" || s == "����" 
                || s == "����" || s == "���") change = true;
              if(curPosCheck2.stringData == positions3[8])
                if(s == "��" || s == "���" || s == "����" 
                || s == "����" || s == "���") change = true;
              if(curPosCheck2.stringData == positions3[9])
                if(s == "���" || s == "����" || s == "���") change = true;
            }
            if(change) {
              playerWithBallPosCheck.addAct("��������� ��� ������ ����",0.8); 
              playerWithBallPosCheck.addAct("������� ����",0.6);
            }
          }
        }
      }
    }
    /**
     * ����� ��� ������ �������� ��������� �������� � ������ �������
     */
    void search() {
      SolutionTreeNode currentNode = root;
      while(true) {
        if(currentNode.type == 1) {
          if(comboBoxBall.getSelectedIndex() == 0)// ��� � ������
          {
            currentNode = currentNode.leftNode;
          } else currentNode = currentNode.rightNode;
        } else if(currentNode.type == 2) {
          //�������� ����� �����������
          if(comboBox_scheme.getSelectedItem() == 
            currentNode.stringData) 
            currentNode = currentNode.leftNode;
          else currentNode = currentNode.rightNode;
        } else if(currentNode.type == 3) {
          if(comboBox_cur_player_pos.getSelectedItem() == 
            currentNode.stringData)
          //�������� ������� �����.������, ���� ��� � ����
          {
        	for(int i=0; i<currentNode.act.length; i++)
          		searchResultAct[i] = currentNode.act[i];
            break;
          } else currentNode = currentNode.rightNode;
        } else if(currentNode.type == 4) {
          if(comboBox_cur_player_pos.getSelectedItem() == 
            currentNode.stringData)
          //�������� ������� �����.������, ���� ��� �� � ����
            currentNode = currentNode.leftNode;
          else currentNode = currentNode.rightNode;
        } else if(currentNode.type == 5) {
          if(comboBox_player_with_the_ball.getSelectedItem() == 
            currentNode.stringData)
          //�������� ������� ������ � �����, ���� ��� �� � �����.������
          {
        	for(int i=0; i<currentNode.act.length; i++)
        		searchResultAct[i] = currentNode.act[i];
            break;
          } else currentNode = currentNode.rightNode;
        } else if(currentNode.type == 0) {
          System.out.println("������ (��� ���� ����� 0)");
          break;
        }
      }
      actCount=currentNode.getActCount();
    }
  }
  /**
   * ����� ��� ��������� ���������� �������������� ������� ��������
   * @return
   */
  public int getOutActCount()
  {
	  return actCount;
  }
  /**
   * ����������� �� ���������
   */
  BaseActions() {
    JFrame jfrm = new JFrame("��������� ��������� ������� ��������");
    jfrm.setLayout(new FlowLayout());
    jfrm.setSize(500, 480);
    jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    String[] method = {
      "������������� �������",
      "������� �������",
    };
    String[] player_with_the_ball = {
      "�",
      "�� �",
    };
    JPanel comboPanel = new JPanel();
    comboPanel.setLayout(new GridLayout(0, 2));
    comboBoxMethod = new JComboBox(method);
    comboBoxBall = new JComboBox(player_with_the_ball);
    comboPanel.add(new JLabel("����� ���������: "));
    comboPanel.add(comboBoxMethod);
    comboBoxMethod.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        reset_counters();
      }
    });

    comboPanel.add(new JLabel("�����, ��������� �����: "));
    comboPanel.add(comboBoxBall);
    comboBoxBall.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if(comboBoxBall.getSelectedIndex() == 0) // ��� � �������� ������
          comboBox_player_with_the_ball.setEnabled(false);
        else comboBox_player_with_the_ball.setEnabled(true);
      }
    });
    comboBox_scheme = new JComboBox(schemes);
    comboPanel.add(new JLabel("����� �����������: "));
    comboPanel.add(comboBox_scheme);
    comboBox_scheme.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        reset_counters();
        comboBox_cur_player_pos.removeAllItems();
        comboBox_player_with_the_ball.removeAllItems();

        // ������������� ������ �������� ������ � ������ � �����
        // (���� ��� �� � �������� ������)
        String temp;
        for(int j = 0; j < SCHEME_COUNT; j++)
          if((String) comboBox_scheme.getSelectedItem() == schemes[j]) {
            for(int i = 0; i < positions1.length; i++) {
              if(j == 0) temp = positions1[i];
              else if(j == 1) temp = positions2[i];
              else temp = positions3[i];
              comboBox_cur_player_pos.addItem(temp);
            }
          }
      }
    });

    JPanel comboPanel2 = new JPanel();
    comboPanel2.setLayout(new GridLayout(0, 2));
    comboBox_cur_player_pos = new JComboBox(positions1);
    comboPanel2.add(new JLabel("������� �������� ������: "));
    comboPanel2.add(comboBox_cur_player_pos);

    // ��������� ������ ������ � ����� (���� ��� �� � �������� ������)
    comboBox_cur_player_pos.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        comboBox_player_with_the_ball.removeAllItems();
        String temp;
        for(int j = 0; j < SCHEME_COUNT; j++)
          if((String) comboBox_scheme.getSelectedItem() == schemes[j]) {
            for(int i = 0; i < positions1.length; i++) {
              if(j == 0) temp = positions1[i];
              else if(j == 1) temp = positions2[i];
              else temp = positions3[i];
              if((String) comboBox_cur_player_pos.getSelectedItem() != temp)
                comboBox_player_with_the_ball.addItem(temp);
            }
          }
      }
    });
    comboBox_player_with_the_ball = new JComboBox(positions1);
    comboBox_player_with_the_ball.setEnabled(false);
    comboPanel2.add(new JLabel("������� ������ � �����: "));
    comboPanel2.add(comboBox_player_with_the_ball);
    comboBox_player_with_the_ball.removeItemAt(0);
    JPanel displayPanel = new JPanel();
    displayPanel.setLayout(new GridLayout(0, 1));
    label = new JLabel("�������� 1: ");
    displayPanel.add(label);
    label.setPreferredSize(new Dimension(400, 30));
    label2 = new JLabel("�������� 2: ");
    displayPanel.add(label2);
    label3 = new JLabel("�������� 3: ");
    displayPanel.add(label3);
    label4 = new JLabel(" ");
    displayPanel.add(label4);
    label5 = new JLabel(" ");
    displayPanel.add(label5);
    label6 = new JLabel(" ");
    displayPanel.add(label6);
	label7 = new JLabel(" ");
    displayPanel.add(label7);
	label8 = new JLabel(" ");
    displayPanel.add(label8);

    JPanel buttonPanel = new JPanel();
    buttonPanel.setLayout(new GridLayout(0, 2));

    resetButton = new JButton("�����");
    resetButton.setActionCommand("reset_counters");
    buttonPanel.add(resetButton);
    resetButton.addActionListener(this);

    generateButton = new JButton("�������������");
    generateButton.setActionCommand("Generate");
    buttonPanel.add(generateButton);
    generateButton.addActionListener(this);
    jfrm.getContentPane().add(comboPanel);
    jfrm.getContentPane().add(comboPanel2);
    String input = "������� ���������� ��� ��� ���������� ����������� ������:";
    jfrm.getContentPane().add(new JLabel(input));
    spinner = new JSpinner(new SpinnerNumberModel(1, 1, 100000, 1));
    spinner.setPreferredSize(new Dimension(70, 20));
    jfrm.getContentPane().add(spinner);
    jfrm.getContentPane().add(displayPanel);
    jfrm.getContentPane().add(buttonPanel);

    //clips = new Environment();
    //String path = "/net/sf/clipsrules/jni/examples/baseactions/resources/test.clp";
    //clips.loadFromResource(path);
    //jfrm.setVisible(true);
    tree = new SolutionTreeNode();
    tree.buildTree();
  }
  /**
   * ����� ��� ������ ����������� ������� ��� ������� �� ������
   * @param ae � ��������� ������������� �������
   */
  public void actionPerformed(
    ActionEvent ae) {
    try {
      onActionPerformed(ae);
    } catch(Exception e) {
      e.printStackTrace();
    }
  }
  /**
   * ����� ��� ����������� ��������������� �������� �������� �� �������
   * @param index ������ �������� �������� � ��������� ������� ��������
   * @return ������� �������� � �������� ��������
   */
  BaseAct getOutAct(int index) {
	 return out_act[index];
  }
  /**
   * ����� ��� ��������� �������� ��������� ��������
   * @throws Exception
   */
  void generate() throws Exception { 
    long start_deffacts_time, end_deffacts_time, start_reset_time, 
         end_reset_time, start_run_time, end_run_time,
		 start_eval_time, end_eval_time;
    double deffacts_time, reset_time, run_time, eval_time;
    int has_the_ball = 0;
    int method;
    if(comboBoxMethod.getSelectedIndex() == 0) method = 0;
    else method = 1;
    if(comboBoxBall.getSelectedIndex() == 0) has_the_ball = 1;
    String assertCmd = "(signs (scheme \"" + 
      (String) comboBox_scheme.getSelectedItem() +
      "\") (position \"" + (String) comboBox_cur_player_pos.getSelectedItem() +
      "\") (has_the_ball " + Integer.toString(has_the_ball) +
      ") (player_with_the_ball_position \"" + 
      (String) comboBox_player_with_the_ball.getSelectedItem() + "\"))";

    for(int k = 1; k <= ((Integer) spinner.getValue()).intValue(); k++) {
      start_deffacts_time = System.nanoTime();
      //if(method == 0) clips.loadFromString("(deffacts start " + assertCmd + ")");
      end_deffacts_time = System.nanoTime();

      start_reset_time = System.nanoTime();
      //if(method == 0) clips.reset();
      end_reset_time = System.nanoTime();

      start_run_time = System.nanoTime();
      if(method == 0) {/*clips.run();*/}
      else tree.search();
      end_run_time = System.nanoTime();

      deffacts_time = (end_deffacts_time - start_deffacts_time) / 1e3;
      reset_time = (end_reset_time - start_reset_time) / 1e3;
      run_time = (end_run_time - start_run_time) / 1e3;
      repeat_count++;   
      if(repeat_count == 1) {
        avg_deffacts_time = deffacts_time;
        avg_reset_time = reset_time;
        avg_run_time = run_time;		
      } else {
        avg_deffacts_time *= (repeat_count - 1);
        avg_deffacts_time = (avg_deffacts_time + reset_time) / repeat_count;
        avg_reset_time *= (repeat_count - 1);
        avg_reset_time = (avg_reset_time + reset_time) / repeat_count;
        avg_run_time *= (repeat_count - 1);
        avg_run_time = (avg_run_time + run_time) / repeat_count;
      } 	  
      if(method == 0) {
        /*String evalStr = "(find-fact ((?f result)) TRUE)";
        FactAddressValue fv;
		start_eval_time = System.nanoTime();
        fv = (FactAddressValue)((MultifieldValue) clips.eval(evalStr)).get(0);
        out_act[0].actName = fv.getFactSlot("act1").toString();
        out_act[1].actName= fv.getFactSlot("act2").toString();
        out_act[2].actName = fv.getFactSlot("act3").toString();
		end_eval_time = System.nanoTime();
		eval_time = (end_eval_time - start_eval_time) / 1e3;
		if(repeat_count == 1) {
		  avg_eval_time = eval_time;
		} else {
		  avg_eval_time *= (repeat_count - 1);
          avg_eval_time = (avg_eval_time + eval_time) / repeat_count;
		}*/
      } else {
    	for(int i=0; i<out_act.length; i++)
    		out_act[i] = tree.getSearchResultAct(i);
      }
	}
    label.setText("�������� 1: " + out_act[0]);
    label2.setText("�������� 2: " + out_act[1]);
    label3.setText("�������� 3: " + out_act[2]);
    avg_deffacts_time = avg_deffacts_time * 10;
    int i = (int) Math.round(avg_deffacts_time);
    avg_deffacts_time = (double) i / 10;
    avg_reset_time = avg_reset_time * 10;
    i = (int) Math.round(avg_reset_time);
    avg_reset_time = (double) i / 10;
    avg_run_time = avg_run_time * 10;
    i = (int) Math.round(avg_run_time);
    avg_run_time = (double) i / 10;
	avg_eval_time = avg_eval_time * 10;
    i = (int) Math.round(avg_eval_time);
    avg_eval_time = (double) i / 10;

    double avg_total = avg_deffacts_time + avg_reset_time + 
	  avg_run_time + avg_eval_time;
    avg_total = avg_total * 10;
    i = (int) Math.round(avg_total);
    avg_total = (double) i / 10;
    if(method == 0) {
      label4.setText("������� ����� (���. ����): " + 
        Double.toString(avg_deffacts_time) + " ���");
      label5.setText("������� ����� (�����): " + 
        Double.toString(avg_reset_time) + " ���");
	  label6.setText("������� ����� ����������� ������: " + 
        Double.toString(avg_run_time) + " ���");
      label7.setText("������� ����� �������� �������: " + 
        Double.toString(avg_eval_time) +
      " ���");
	  label8.setText("�����: " + 
        Double.toString(avg_total) + " ���");
    } else {
      label4.setText("������� ����� ������: " + 
        Double.toString(avg_run_time) + " ���");
      label5.setText(" ");
    }
  }
  /**
   * ����� ��� ������ ���������, ��������� � ��������� �������
   */
  private void reset_counters() {
    avg_deffacts_time = 0.0;
    avg_reset_time = 0.0;
    avg_run_time = 0.0;
	avg_eval_time = 0.0;
    repeat_count = 0;
    label.setText("�������� 1:");
    label2.setText("�������� 2:");
    label3.setText("�������� 3:");
    label4.setText(" ");
    label5.setText(" ");
    label6.setText(" ");
	label7.setText(" ");
	label8.setText(" ");
  }
  /**
   * ���������� ������������� ������� (������� �� ������)
   * @param ae �������� ������������� �������
   * @throws Exception
   */
  private void onActionPerformed(ActionEvent ae) throws Exception {
    if(ae.getActionCommand().equals("Generate")) {
      generate();
    } else if(ae.getActionCommand().equals("reset_counters")) {
      reset_counters();
    }
  }

  /*
  public static void main(String args[]) {
    // ������� ���������
    SwingUtilities.invokeLater(
      new Runnable() {
        public void run() {
          new BaseActions();
        }
      });
  }*/
}