package planningSubsystem;

import java.util.ArrayList;

import planningSubsystem.Constants.ActorType;

/**
 * Класс данных об окружающих игроках
 * @author Евгений Здоровец
 */
public class PlayerInfo {
	private RelativeCoordinate coordinates;
	private int number;
	private ActorType actorType = ActorType.UNINDENTIFIED; // тип игрока
	private ArrayList<TestActParams> actions; // выполняемые игроком действия
	/**
	 * Конструктор по умолчанию
	 */
	public PlayerInfo() {
		
	}
	/**
	 * Конструктор
	 * @param direction направление на игрока
	 * @param distance расстояние до игрока
	 * @param number номер игрока
	 * @param actorType тип игрока
	 */
	public PlayerInfo(double direction, double distance, int number, ActorType actorType){
		coordinates = new RelativeCoordinate(direction, distance);
		this.number = number;
		this.actorType = actorType;
	}
	/**
	 * Метод для получения относительной координаты
	 * @return относительная координата
	 */
	public RelativeCoordinate getRelativeCoordinates(){
		return coordinates;
	}
	/**
	 * Метод для задания относительной координаты
	 * @param coord координата для присваивания
	 */
	public void setRelativeCoordinates(RelativeCoordinate coord){ 
		this.coordinates = coord;
	}
	/**
	 * Метод для получения номера игрока
	 * @return номер игрока
	 */
	public int getNumber(){
		return number;
	}
	/**
	 * Метод для задания номера игрока
	 * @param number номер для присваивания
	 */
	public void setNumber(int number){
		this.number = number;
	}
	/**
	 * Метод для получения типа актора
	 * @return тип актора (партнёр или оппонент)
	 */
	public ActorType getActorType(){
		return actorType;
	}
	/**
	 * Метод для задания типа актора
	 * @param actorType тип актора (партнёр или оппонент)
	 */
	public void setActorType(ActorType actorType){
		this.actorType = actorType;
	}
	/**
	 * Метод для получения выполняемых игроком действий
	 * @return выполняемые игроком действия
	 */
	public ArrayList<TestActParams> getActions(){
		return actions;
	}
	/**
	 * Метод для получения отдельного выполняемого игроком действия
	 * @return выполняемое игроком действие
	 */
	public TestActParams getActionAt(int index){
		return actions.get(index);
	}
	/**
	 * Метод для задания действий игрока
	 * @param number действия для присваивания
	 */
	public void setActions(ArrayList<TestActParams> actions){
		this.actions = actions;
	}
}
