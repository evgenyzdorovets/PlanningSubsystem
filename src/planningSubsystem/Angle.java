package planningSubsystem;
public class Angle {
	/**
	 * Метод для корректировки угла направления в пределах (-180; 180]
	 * @param angle угол
	 * @return угол в пределах (-180; 180]
	 */
	public static Double mod180(Double angle)
	{
		while (angle > 180.0)
			angle -= 360.0;
		while (angle <= -180.0)
			angle += 360.0;
		return angle;
	}
	/**
	 * Метод для определения, различаются ли два угла не более чем на заданное значение
	 * @param a1 первый угол
	 * @param a2 второй угол
	 * @param amount заданное значение
	 * @return различаются ли два угла
	 */
	public static boolean anglesDifferLessThan(double a1, double a2, double amount)
	{
		double diff1 = Math.abs(a1 - a2); 
		double diff2 = 360 - diff1;
		return diff1 < amount || diff2 < amount;
	}
}
