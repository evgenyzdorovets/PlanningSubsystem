package planningSubsystem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import objects.Ball;
import objects.Command;
import objects.Player;
import objects.Velocity;
import planningSubsystem.Constants.OurTeamSide;
import server.ServerImitator;

/**
 * Класс входных данных
 * @author Евгений Здоровец
 */
public class InputData implements Serializable {
	// входные данные от ПОО:
	// номер такта действия
	private int currentTact;
	// владелец мяча
	private String ballOwner;
	// схема расстановки
	private String scheme;
	// позиция планирующего игрока
	private String currentPlayerPosition;
	// информация об игроках-партнёрах по команде
	private ArrayList<PlayerInfo> teamMateInfoIn = new ArrayList<PlayerInfo>();
	// информация об игроках-оппонентах
	private ArrayList<PlayerInfo> enemyInfoIn = new ArrayList<PlayerInfo>();
	// координата планирующего игрока
	private AbsoluteCoordinate myCoord = new AbsoluteCoordinate();
	// угол поворота планирующего игрока
	private double myNeckPosition;
	// смещение мяча относительно планирующего игрока в начальный момент времени
	private AbsoluteCoordinate startBallOffset = new AbsoluteCoordinate();
	// сторона футбольного поля команды планирующего игрока 
	private OurTeamSide ourSide;
	// установка подробного лога
	private boolean verboseLog;
	/**
	 * Конструктор по умолчанию
	 */
	public InputData() {
		
	}
	/**
	 * Конструктор для копирования объекта
	 */
	public InputData(ArrayList<PlayerInfo> teamMateInfoIn, ArrayList<PlayerInfo> enemyInfoIn, AbsoluteCoordinate myCoord, double myNeckPosition) {
		for (int i = 0; i < teamMateInfoIn.size(); i++)
			this.teamMateInfoIn.add(new PlayerInfo(teamMateInfoIn.get(i).getRelativeCoordinates().getDirection(),
					teamMateInfoIn.get(i).getRelativeCoordinates().getDistance(), 
					teamMateInfoIn.get(i).getNumber(), 
					teamMateInfoIn.get(i).getActorType()));
		for (int i = 0; i < enemyInfoIn.size(); i++)
			this.enemyInfoIn.add(new PlayerInfo(enemyInfoIn.get(i).getRelativeCoordinates().getDirection(),
					enemyInfoIn.get(i).getRelativeCoordinates().getDistance(), 
					enemyInfoIn.get(i).getNumber(), 
					enemyInfoIn.get(i).getActorType()));
		this.myCoord.setX(myCoord.getX());
		this.myCoord.setY(myCoord.getY());
		this.myNeckPosition = myNeckPosition;
	}
	/**
	 * Метод для определения, является ли лог подробным
	 * @return да/нет
	 */
	public boolean isVerboseLog() {
		return verboseLog;
	}
	/**
	 * Метод для задания уровня лога
	 * @param verboseLog подробный лог (да/нет)
	 */
	public void setVerboseLog(boolean verboseLog) {
		this.verboseLog = verboseLog;
	}
	/**
	 * Метод для получения владельца мяча
	 * @return владелец мяча
	 */
	public String getBallOwner() {
		return ballOwner;
	}
	/**
	 * Метод для задания владельца мяча
	 * @param ballOwner владелец мяча
	 */
	public void setBallOwner(String ballOwner) {
		this.ballOwner = ballOwner;
	}
	/**
	 * Метод для получения схемы расстановки
	 * @return схема расстановки
	 */
	public String getScheme() {
		return scheme;
	}
	/**
	 * Метод для задания схемы расстановки
	 * @param scheme схема расстановки
	 */
	public void setScheme(String scheme) {
		this.scheme = scheme;
	}
	/**
	 * Метод для получения позиции планирующего игрока в схеме расстановки
	 * @return позиция планирующего игрока в схеме расстановки
	 */
	public String getCurrentPlayerPosition() {
		return currentPlayerPosition;
	}
	/**
	 * Метод для задания позиции планирующего игрока в схеме расстановки
	 * @param currentPlayerPosition позиция планирующего игрока в схеме расстановки
	 */
	public void setCurrentPlayerPosition(String currentPlayerPosition) {
		this.currentPlayerPosition = currentPlayerPosition;
	}
	/**
	 * Метод для получения номера текущего такта
	 * @return номер текущего такта
	 */
	public int getCurrentTact() {
		return currentTact;
	}
	/**
	 * Метод для задания номера текущего такта
	 * @param currentTact номер текущего такта
	 */
	public void setCurrentTact(int currentTact) {
		this.currentTact = currentTact;
	}
	/**
	 * Метод для получения абсолютной координаты планирующего игрока
	 * @return абсолютная координата планирующего игрока
	 */
	public AbsoluteCoordinate getMyCoord() {
		return myCoord;
	}
	/**
	 * Метод для задания абсолютной координаты планирующего игрока
	 * @param myCoord абсолютная координата планирующего игрока
	 */
	public void setMyCoord(AbsoluteCoordinate myCoord) {
		this.myCoord = myCoord;
	}
	/**
	 * Метод для получения угла поворота планирующего игрока
	 * @return угол поворота планирующего игрока
	 */
	public double getMyNeckPosition() {
		return myNeckPosition;
	}
	/**
	 * Метод для задания угла поворота планирующего игрока
	 * @param myNeckPosition угол поворота планирующего игрока
	 */
	public void setMyNeckPosition(double myNeckPosition) {
		this.myNeckPosition = myNeckPosition;
	}
	/**
	 * Метод для получения смещения мяча относительно планирующего игрока в начальный момент времени
	 * @return смещение мяча относительно планирующего игрока в начальный момент времени
	 */
	public AbsoluteCoordinate getStartBallOffset() {
		return startBallOffset;
	}
	/**
	 * Метод для задания смещения мяча относительно планирующего игрока в начальный момент времени
	 * @param startBallOffset смещение мяча относительно планирующего игрока в начальный момент времени
	 */
	public void setStartBallOffset(AbsoluteCoordinate startBallOffset) {
		this.startBallOffset = startBallOffset;
	}
	/**
	 * Метод для получения расположения команды планирующего игрока
	 * @return расположение команды планирующего игрока (слева или справа)
	 */
	public OurTeamSide getOurSide() {
		return ourSide;
	}
	/**
	 * Метод для задания расположения команды планирующего игрока
	 * @param ourSide расположение команды планирующего игрока (слева или справа)
	 */
	public void setOurSide(OurTeamSide ourSide) {
		this.ourSide = ourSide;
	}
	/**
	 * Метод для получения информации об игроках-партнёрах
	 * @return массив информации об игроках-партнёрах
	 */
	public ArrayList<PlayerInfo> getTeamMateInfoIn() {
		return teamMateInfoIn;
	}
	/**
	 * Метод для задания информации об игроках-партнёрах
	 * @param teamMateInfoIn массив информации об игроках-партнёрах
	 */
	public void setTeamMateInfoIn(ArrayList<PlayerInfo> teamMateInfoIn) {
		this.teamMateInfoIn = teamMateInfoIn;
	}
	/**
	 * Метод для получения информации об игроках-оппонентах
	 * @return массив информации об игроках-оппонентах
	 */
	public ArrayList<PlayerInfo> getEnemyInfoIn() {
		return enemyInfoIn;
	}
	/**
	 * Метод для задания информации об игроках-оппонентах
	 * @param enemyInfoIn массив информации об игроках-оппонентах
	 */
	public void setEnemyInfoIn(ArrayList<PlayerInfo> enemyInfoIn) {
		this.enemyInfoIn = enemyInfoIn;
	}
	/**
	 * Метод для заполнения экземпляров классов подсистемы исполнения
	 * @param serverImitator пустой имитатор сервера
	 * @param playerList пустой список игроков
	 */
	public void fill(ServerImitator serverImitator, List<Player> playerList) {
		RelativeCoordinate relative;
        AbsoluteCoordinate absoluteCoord;
		for (int i = -1; i < teamMateInfoIn.size() + enemyInfoIn.size(); i++) {
	        Player player = new Player();
	        if (i < teamMateInfoIn.size())
	        	player.setCommand(Command.OUR);
	        else
	        	player.setCommand(Command.OPPOSSITE);
	        player.setGlobalBodyAngle(0);
	        if (i == -1) { 
	        	// задание начальной координаты для планирующего игрока
	        	absoluteCoord = getMyCoord();
	        	player.setGlobalBodyAngle(getMyNeckPosition());
	        }
	        else if (i < teamMateInfoIn.size()) { 
	        	// задание начальной координаты для игроков-партнёров
	        	relative = teamMateInfoIn.get(i).getRelativeCoordinates();
	        	absoluteCoord = AbsoluteCoordinate.relativeToAbsoluteWithBase(getMyCoord(), getMyNeckPosition(), relative.getDirection(), relative.getDistance());
	        }
	        else { 
	        	// задание начальной координаты для игроков-оппонентов
	        	relative = enemyInfoIn.get(i - teamMateInfoIn.size()).getRelativeCoordinates();
		        absoluteCoord = AbsoluteCoordinate.relativeToAbsoluteWithBase(getMyCoord(), getMyNeckPosition(), relative.getDirection(), relative.getDistance());
	        }
	        player.setPosX(absoluteCoord.toServerFormat().getX());
	        player.setPosY(absoluteCoord.toServerFormat().getY());
	        playerList.add(player);
	        serverImitator.connectToServer(player);
		}
		// задание начального расположения мяча
		Ball ball = serverImitator.getBall();
		AbsoluteCoordinate ballCoord = new AbsoluteCoordinate();
		ballCoord.setX(getMyCoord().getX() + getStartBallOffset().getX());
		ballCoord.setY(getMyCoord().getY() + getStartBallOffset().getY());
		ball.setPosX(ballCoord.toServerFormat().getX());
		ball.setPosY(ballCoord.toServerFormat().getY());
		Velocity ballVelocity = new Velocity();
		ballVelocity.setX(0);
		ballVelocity.setY(0);
		ball.setGlobalVelocity(ballVelocity);
		playerList.get(0).setGlobalBodyAngle(getMyNeckPosition());
	}
}
