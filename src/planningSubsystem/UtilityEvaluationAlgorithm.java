package planningSubsystem;

import java.util.ArrayList;

import javax.swing.JTextArea;

import objects.Ball;
import planningSubsystem.Constants.ActionTypes;
import planningSubsystem.Constants.ActorType;
import planningSubsystem.Constants.OurTeamSide;
import server.ServerParameters;

/**
 * Класс алгоритма оценки полезности действий
 * @author Евгений Здоровец
 */
public class UtilityEvaluationAlgorithm {
	private ArrayList<ActorAct> myActions = new ArrayList<ActorAct>();
	private int[] acts = { 2, 3, 3, 3, 2, 1 }; // количество действий по типам действий
	private int[] actionCount;
	private int length;
	private ArrayList<ArrayList<PlayerInfo>> teamMateInfo;
	private ArrayList<ArrayList<PlayerInfo>> enemyInfo;
	private ArrayList<Integer> teamMatesCount;
	private ArrayList<Integer> enemyCount;
	private Situation[] situations;
	private JTextArea logArea;
	private String msg;
	/**
	 * Конструктор
	 * @param logArea окно, куда будет выводиться лог
	 */
	public UtilityEvaluationAlgorithm(JTextArea logArea) {
		this.logArea = logArea;
	}
	/**
	 * Метод для получения информации об игроках-партнёрах
	 * @return массив информации об игроках-партнёрах
	 */
	public ArrayList<ArrayList<PlayerInfo>> getTeamMateInfo() {
		return teamMateInfo;
	}
	/**
	 * Метод для получения информации об игроках-оппонентах
	 * @return массив информации об игроках-оппонентах
	 */
	public ArrayList<ArrayList<PlayerInfo>> getEnemyInfo() {
		return enemyInfo;
	}
	/**
	 * Метод для получения массива действий планирующего игрока
	 * @return массив действий планирующего игрока
	 */
	public ArrayList<ActorAct> getMyActions() {
		return myActions;
	}
	/**
	 * Метод для оценки полезности прогнозируемых ситуаций и действий планирующего игрока
	 * @param inputData входных данных
	 * @return действие с наибольшей полезностью
	 */
	public ActorAct evaluatePredictedSituationsUtility(InputData inputData, Ball ball) {
		double actUtility, additionalUtility;
		ArrayList<Double> planningPlayerActUtility = new ArrayList<Double>();
		for (int i = 0; i < myActions.size(); i++)
			planningPlayerActUtility.add(0.0);
		for (int i = 0; i < length; i++) {
			if (situations[i] != null) {
				int planningPlayerActType = situations[i].getAct(0).getBaseActionType();
				actUtility = 0;
				additionalUtility = 0;
				double teammateMinDistanceToBall = 9999;
				double enemiesMinDistanceToBall = 9999;
				for (int m = 0; m < situations[i].getActorCount(); m++) {
					actUtility += situations[i].getAct(m).getBaseActUtility();
					ActorAct myAction = myActions.get(situations[i].getActIndex());
					ActorAct actorAction = situations[i].getAct(m);
					
					int actorMultiplier = Constants.getActorMultiplier(actorAction.getActorType());
					if (ActionTypes.values()[planningPlayerActType] == ActionTypes.PASS ||
						ActionTypes.values()[planningPlayerActType] == ActionTypes.KICK_TO_GATE) {
							if (myAction.isConcretized()) {
							// вычисление минимального расстояния до мяча отдельно по всем партнёрам и оппонентам
							// рассматриваются только те партнёры и оппоненты, которые находятся в примерном направлении подачи мяча 
							AbsoluteCoordinate ballAbsCoord = new AbsoluteCoordinate(ball.getPosX(), ball.getPosY()).toSupportedFormat();
							RelativeCoordinate ballRelCoord = RelativeCoordinate.absoluteToRelative(inputData.getMyCoord(), ballAbsCoord, inputData.getMyNeckPosition());
							double directionDelta = Angle.mod180(actorAction.getActParams().getDirection() - ballRelCoord.getDirection());
							double distanceDelta = actorAction.getActParams().getDistance() - ballRelCoord.getDistance();
							RelativeCoordinate enemyToBallCoord = new RelativeCoordinate(directionDelta, distanceDelta);
							if (opponentIsInWay(myAction, enemyToBallCoord.getDirection(), enemyToBallCoord.getDistance(), inputData)) {
								System.out.println(enemyToBallCoord.getDirection()+" "+enemyToBallCoord.getDistance());
								AbsoluteCoordinate actorCoord = AbsoluteCoordinate.relativeToAbsoluteWithBase(inputData.getMyCoord(), inputData.getMyNeckPosition(), directionDelta, distanceDelta);
								double actorDistToBall = AbsoluteCoordinate.getDistanceBetweenCoords(ballAbsCoord, actorCoord);
								if (actorMultiplier == -1)
									enemiesMinDistanceToBall = Math.min(enemiesMinDistanceToBall, actorDistToBall);
								else
									teammateMinDistanceToBall = Math.min(teammateMinDistanceToBall, actorDistToBall);
							}
						}
					}
					
					if (ActionTypes.values()[planningPlayerActType] == ActionTypes.DRIBBLING) {
						// Оценка полезности действия типа "ведение мяча"
						double directionUtility = Math.sin(Math.toRadians(Angle.mod180(
								situations[i].getAct(m).getActParams().getDirection() + inputData.getMyNeckPosition())));
						if (inputData.getOurSide() == OurTeamSide.RIGHT)
							directionUtility = (-directionUtility);
						additionalUtility += actorMultiplier * 0.01	* directionUtility;
					} else if (ActionTypes.values()[planningPlayerActType] == ActionTypes.PASS) {
						// Оценка полезности действия "пас"
						double k = 0.01;
						//if (myAction.getActorNumber() == actorAction.getActorNumber())
						//	k = 0.02;
						additionalUtility -= actorMultiplier * k
								* actorAction.getActParams().getDistance();
						if (myAction.getActParamsAbs().getX() < Constants.MAX_X/2 && inputData.getOurSide() == OurTeamSide.LEFT)
							additionalUtility = -1;
						if (myAction.getActParamsAbs().getX() > Constants.MAX_X/2 && inputData.getOurSide() == OurTeamSide.RIGHT)
							additionalUtility = -1;
					}
				}
				actUtility /= situations[i].getActorCount();
				if (ActionTypes.values()[planningPlayerActType] == ActionTypes.KICK_TO_GATE)
					additionalUtility = 1 - situations[i].getAct(0).getActParams().getDistance() / 20;
				int koef = 0;
				if (teammateMinDistanceToBall > enemiesMinDistanceToBall)
					koef = 1;
				double predictedUtility = (actUtility + additionalUtility) / (koef + 1);
				if (predictedUtility > 1)
					predictedUtility = 1.0;
				else if (predictedUtility < 0)
					predictedUtility = 0.0;
				predictedUtility = Math.floor(predictedUtility * 10000) / 10000;
				situations[i].setPredictedSituationUtility(predictedUtility);
				planningPlayerActUtility.set(situations[i].getActIndex(), predictedUtility);
				double situationUtilty = planningPlayerActUtility.get(situations[i].getActIndex());
				if(situationUtilty > 1e-9)
					planningPlayerActUtility.set(situations[i].getActIndex(), 
							Math.min(situationUtilty, predictedUtility));
			}
		}
		int maxActUtilityIndex = 0;
		double max = 0.0;
		for (int i = 0; i < myActions.size(); i++) {
			if (max < planningPlayerActUtility.get(i) && myActions.get(i).isConcretized()) {
				maxActUtilityIndex = i;
				max = planningPlayerActUtility.get(i);
			}
			if (inputData.isVerboseLog()) {
				msg = (i + 1) + ". Действие планирующего агента:\n (" + myActions.get(i) + ").";
				if (myActions.get(i).isConcretized())
					msg = msg + " Полезность: " + planningPlayerActUtility.get(i);
				else
					msg = msg + "\n(не конкретизировано)";
				if(logArea != null)
					logArea.setText(logArea.getText() + msg + "\n");
				else
					System.out.println(msg);
			}
		}
		if (myActions.get(maxActUtilityIndex).isConcretized()) {
			msg = "Действие с максимальной полезностью:\n("
				+ myActions.get(maxActUtilityIndex) + "):"
				+ planningPlayerActUtility.get(maxActUtilityIndex);
			if(logArea != null)
				logArea.setText(logArea.getText() + msg + "\n");
			else
				System.out.println(msg);
		}
		else {
			msg = "Нет конкретизированных действий.";
			if(logArea != null)
				logArea.setText(logArea.getText() + msg + "\n");
			else
				System.out.println(msg);
		}
		return myActions.get(maxActUtilityIndex);
	}
	/**
	 * Метод для проверки ситуаций на уникальность
	 * @return уникальны ли построенные прогнозируемые ситуации
	 */
	public boolean checkIfUnique() {
		for (int i = 0; i < length; i++) {
			loop: for (int k = i + 1; k < length; k++) {
				if (i != k && situations[i].getActorCount() == situations[k].getActorCount()) {
					for (int m = 0; m < situations[i].getActorCount(); m++) {
						if (!situations[i].getAct(m).toString().equals(situations[k].getAct(m).toString()))
							continue loop;
					}
					if (situations[i].getActIndex() == situations[k].getActIndex())
						return false;
				}
			}
		}
		return true;
	}
	/**
	 * Главный метод, последовательно вызывающий другие методы
	 * @param inputData входные данные
	 * @return true
	 */
	public boolean predictSituations(InputData inputData) {
		teamMateInfo = new ArrayList<ArrayList<PlayerInfo>>();
		enemyInfo = new ArrayList<ArrayList<PlayerInfo>>();
		teamMatesCount = new ArrayList<Integer>();
		enemyCount = new ArrayList<Integer>();
		length = 0;
		if (inputData.getCurrentTact() == 1) // первый такт текущего действия
		{
			BaseActions baseActions = new BaseActions();
			baseActions.comboBoxMethod.setSelectedIndex(1);
			baseActions.comboBoxBall.setSelectedItem(inputData.getBallOwner()); // установка владельца мяча
			baseActions.comboBox_scheme.setSelectedItem(inputData.getScheme());
			baseActions.comboBox_cur_player_pos.setSelectedItem(inputData.getCurrentPlayerPosition());
			// генерация базового множества действий
			try {
				baseActions.generate();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
			for (int i = 0; i < baseActions.getOutActCount(); i++) {
				ActorAct actionToAdd = new ActorAct(baseActions.getOutAct(i));
				myActions.add(actionToAdd);
			}
		}
		int size = myActions.size();
		for (int i = 0; i < size; i++)
			while (myActions.get(i).equals("Нет"))
				myActions.remove(i); // удаление пустых действий
		size = myActions.size();
		actionCount = new int[2 * size];
		RelativeCoordinate gateCoord = RelativeCoordinate.calculateGateRelativeCoordinate(inputData);
		getActualActorSet(inputData, gateCoord);
		msg = "Номер такта действия = " + inputData.getCurrentTact() + ":";
		if(logArea != null)
			logArea.setText(logArea.getText() + msg + "\n");
		else
			System.out.println(msg);
		buildPredictedSituations(inputData.getTeamMateInfoIn(), inputData.getEnemyInfoIn(), gateCoord);
		return true;
	}
	/**
	 * Метод для получения множества актуальных акторов
	 * @param inputData входные данные
	 * @param gateCoord относительная координата на ворота противника
	 */
	private void getActualActorSet(InputData inputData, RelativeCoordinate gateCoord) {
		double baseAngle;
		for (int i = 0; i < myActions.size(); i++) {
			boolean contains = false;
			int containsIndex = -1;
			for (int j = 0; j < Constants.sectoralDirections.length; j++)
				if (myActions.get(i).getActName().contains(Constants.sectoralDirections[j])) {
					contains = true;
					containsIndex = j;
				}
				//{
					ArrayList<PlayerInfo> temp = new ArrayList<PlayerInfo>();
					int count = 0;
					if (contains) {
					double concrDirection = 0.0;
					/*if (myActions.get(i).isConcretized()
							&& myActions.get(i).getActName().contains(Constants.actionTypes[0])) {
						concrDirection = myActions.get(i).getActParams().getDirection();
						baseAngle = Angle.mod180(Angle.mod180(concrDirection));
					} else*/ {
						baseAngle = 0.0;
						if (inputData.getOurSide() == OurTeamSide.LEFT)
							baseAngle = Angle.mod180(Constants.sectoralAngles_FOR_LEFT_SIDE[containsIndex]);
						if (inputData.getOurSide() == OurTeamSide.RIGHT)
							baseAngle = Angle.mod180(Constants.sectoralAngles_FOR_RIGHT_SIDE[containsIndex]);
					}
					}
					else baseAngle = gateCoord.getDirection();
					double teamMateDirection;
					double teamMateDirectionWithNeckPosition;
					boolean teamMateBelongsToActualActors,
							enemyBelongsToActualActors;
					for (int k = 0; k < inputData.getTeamMateInfoIn().size(); k++) {
						teamMateDirection = inputData.getTeamMateInfoIn().get(k).getRelativeCoordinates().getDirection();
						teamMateDirectionWithNeckPosition = Angle.mod180(teamMateDirection + inputData.getMyNeckPosition());
						teamMateBelongsToActualActors = false;
						/*if (Angle.anglesDifferLessThan(teamMateDirectionWithNeckPosition, baseAngle, 60)
								|| inputData.teamMateInfoIn.get(k).getRelativeCoordinates().getDistance() <= 20.0
										&& Angle.anglesDifferLessThan(teamMateDirectionWithNeckPosition, baseAngle, 135.0))*/ 
						//if (myActions.get(i).getActName().contains(Constants.actionTypes[1]))
							if (Angle.anglesDifferLessThan(teamMateDirectionWithNeckPosition, baseAngle, 90))
								teamMateBelongsToActualActors = true;
						if(teamMateBelongsToActualActors){
							temp.add(new PlayerInfo(teamMateDirection,
									inputData.getTeamMateInfoIn().get(k).getRelativeCoordinates().getDistance(), inputData.getTeamMateInfoIn().get(k).getNumber(),
									ActorType.TEAMMATE));
							count++;
						}
					}
					teamMateInfo.add(temp);
					if (myActions.get(i).getActName().contains(ActionTypes.DRIBBLING.toString()))
						actionCount[2 * i] = (int) Math.pow(acts[0], count);
					else if (myActions.get(i).getActName().contains(ActionTypes.PASS.toString()))
						actionCount[2 * i] = (int) Math.pow(acts[2], count);
					else if (myActions.get(i).getActName().contains(ActionTypes.KICK_TO_GATE.toString()))
						actionCount[2 * i] = (int) Math.pow(acts[4], count);
					teamMatesCount.add(count);

					temp = new ArrayList<PlayerInfo>();
					count = 0;
					double enemyDirection, enemyDistance;
					double enemyDirectionWithNeckPosition;
					for (int k = 0; k < inputData.getEnemyInfoIn().size(); k++) {
						enemyDirection = inputData.getEnemyInfoIn().get(k).getRelativeCoordinates().getDirection();
						enemyDistance = inputData.getEnemyInfoIn().get(k).getRelativeCoordinates().getDistance();
						enemyDirectionWithNeckPosition = Angle.mod180(enemyDirection + inputData.getMyNeckPosition());
						enemyBelongsToActualActors = false;
						/*if (Angle.anglesDifferLessThan(enemyDirectionWithNeckPosition, baseAngle, 60)
								|| inputData.enemyInfoIn.get(k).getRelativeCoordinates().getDistance() <= 20.0
										&& Angle.anglesDifferLessThan(enemyDirectionWithNeckPosition, baseAngle, 135.0)) */
						if (Angle.anglesDifferLessThan(enemyDirectionWithNeckPosition, baseAngle, 90))
							enemyBelongsToActualActors = true;
						if(enemyBelongsToActualActors) {
							temp.add(new PlayerInfo(enemyDirection,
									inputData.getEnemyInfoIn().get(k).getRelativeCoordinates().getDistance(), inputData.getEnemyInfoIn().get(k).getNumber(),
									ActorType.ENEMY));
							count++;
						}
					}
					enemyInfo.add(temp);
					if (myActions.get(i).getActName().contains(ActionTypes.DRIBBLING.toString()))
						actionCount[2 * i + 1] = (int) Math.pow(acts[1], count);
					else if (myActions.get(i).getActName().contains(ActionTypes.PASS.toString()))
						actionCount[2 * i + 1] = (int) Math.pow(acts[3], count);
					else if (myActions.get(i).getActName().contains(ActionTypes.KICK_TO_GATE.toString()))
						actionCount[2 * i + 1] = (int) Math.pow(acts[5], count);
					enemyCount.add(count);
					length += actionCount[2 * i] * actionCount[2 * i + 1];
		}
	}
	/**
	 * Метод для создания ситуации с заданным индексом и заданным количеством комбинаций действий актуальных акторов
	 * @param i индекс в массиве количества актуальных акторов
	 * @param combinationCounter количество комбинаций действий актуальных акторов
	 */
	private void allocateSituation(int i, int combinationCounter) {
		if (situations[combinationCounter] == null) {
			situations[combinationCounter] = new Situation();
			situations[combinationCounter].allocateActs(teamMatesCount.get(i) + enemyCount.get(i));
		}
	}
	/**
	 * Метод для генерации действий актуального актора в определённой ситуации
	 * @param actIndex тип действия
	 * @param infoIn массив информации об актуальных акторах
	 * @param currentActor индекс текущего актора
	 * @param gateCoord относительная координата на ворота противника
	 * @return массив действий актуального актора
	 */
	private ActorAct[] generateActualActorActs(int actIndex, ArrayList<PlayerInfo> infoIn, int currentActor,
			RelativeCoordinate gateCoord) {
		ActorAct[] actualActorActs = new ActorAct[acts[actIndex]];
		if (actIndex == 0) {
			/*
			 * генерация вариантов действий партнёров для базового действия
			 * планирующего игрока "Ведение мяча"
			 */
			actualActorActs[0] = new ActorAct(
					new BaseAct("Создать численное преимущество в зоне партнёра с мячом", 0.7), 0, ActorType.TEAMMATE,
					infoIn.get(currentActor));
			actualActorActs[1] = new ActorAct(new BaseAct("Отвлечь оппонента", 0.65), 0, ActorType.TEAMMATE,
					infoIn.get(currentActor));
		} else if (actIndex == 1) {
			/*
			 * генерация вариантов действий оппонентов для базового действия
			 * планирующего игрока "Ведение мяча"
			 */
			actualActorActs[0] = new ActorAct(new BaseAct("Отобрать мяч", 0.5), 0, ActorType.ENEMY,
					infoIn.get(currentActor));
			actualActorActs[1] = new ActorAct(
					new BaseAct("Противодействовать выходу оппонента с мячом на острую позицию", 0.6), 0,
					ActorType.ENEMY, infoIn.get(currentActor));
			actualActorActs[2] = new ActorAct(new BaseAct("Закрыть опекаемого игрока", 0.7), 0, ActorType.ENEMY,
					infoIn.get(currentActor));
		} else if (actIndex == 2) {
			/*
			 * генерация вариантов действий партнёров для базового действия
			 * планирующего игрока "пас"
			 */
			actualActorActs[0] = new ActorAct(new BaseAct("Открыться для приёма паса", 0.8), 1, ActorType.TEAMMATE,
					infoIn.get(currentActor));
			actualActorActs[1] = new ActorAct(
					new BaseAct("Создать численное преимущество в зоне партнёра с мячом", 0.7), 1, ActorType.TEAMMATE,
					infoIn.get(currentActor));
			actualActorActs[2] = new ActorAct(new BaseAct("Отвлечь оппонента", 0.65), 1, ActorType.TEAMMATE,
					infoIn.get(currentActor));
		} else if (actIndex == 3) {
			/*
			 * генерация вариантов действий оппонентов для базового действия
			 * планирующего игрока "пас"
			 */
			actualActorActs[0] = new ActorAct(new BaseAct("Попытаться перехватить мяч", 0.5), 1, ActorType.ENEMY,
					infoIn.get(currentActor));
			actualActorActs[1] = new ActorAct(new BaseAct("Попытаться воспрепятствовать острой передаче мяча", 0.6), 1,
					ActorType.ENEMY, infoIn.get(currentActor));
			actualActorActs[2] = new ActorAct(new BaseAct("Закрыть игрока, которому передаётся пас", 0.7), 1,
					ActorType.ENEMY, infoIn.get(currentActor));
		} else if (actIndex == 4) {
			/*
			 * генерация вариантов действий партнёров для базового действия
			 * планирующего игрока "удар по воротам"
			 */
			actualActorActs[0] = new ActorAct(
					new BaseAct("Создать численное преимущество в зоне партнёра с мячом", 0.9), 2, ActorType.TEAMMATE,
					gateCoord);
			actualActorActs[1] = new ActorAct(new BaseAct("Отвлечь оппонента", 0.8), 2, ActorType.TEAMMATE,
					gateCoord);
		} else if (actIndex == 5) {
			/*
			 * генерация вариантов действий оппонентов для базового действия
			 * планирующего игрока "удар по воротам"
			 */
			actualActorActs[0] = new ActorAct(new BaseAct("Попытаться воспрепятствовать нанесению удара", 0.5), 2,
					ActorType.ENEMY, gateCoord);
		}
		return actualActorActs;
	}
	/**
	 * Метод для получения частоты, с которой будет повторяться заданное действие в ситуации
	 * @param actIndex индекс действия
	 * @param i в массиве количества актуальных акторов
	 * @param currentActor индекс текущего актора
	 * @return частота, с которой будет повторяться заданное действие в ситуации
	 */
	private int calculateFrequency(int actIndex, int i, int currentActor) {
		if (actIndex == 0 || actIndex == 2 || actIndex == 4)
			return (int) Math.pow(acts[actIndex], teamMatesCount.get(i) - (1 + currentActor));
		else if (actIndex == 1 || actIndex == 5)
			return (int) Math.pow(acts[actIndex], enemyCount.get(i) - (1 + currentActor));
		else if (actIndex == 3)
			return (int) Math.pow(acts[actIndex], teamMatesCount.get(i) + enemyCount.get(i) - (1 + currentActor));
		return 0;
	}
	/**
	 * Метод для построения прогнозируемых ситуаций
	 * @param teamMateInfoIn массив информации об игроках-партнёрах
	 * @param enemyInfoIn массив информации об игроках-оппонентах
	 * @param gateCoord относительная координата на ворота противника
	 * @return true
	 */
	private boolean buildPredictedSituations(ArrayList<PlayerInfo> teamMateInfoIn, ArrayList<PlayerInfo> enemyInfoIn,
			RelativeCoordinate gateCoord) {
		situations = new Situation[length];
		for (int j = 0; j < length; j++)
			situations[j] = null;
		// построить множества базовых действий актуальных агентов
		// в предсказываемых ситуациях
		int combinationCounterAtStart = 0, combinationCounter = 0;
		for (int i = 0; i < myActions.size(); i++) {
			if (!myActions.get(i).equals("Нет")) {
				int currentActor;
				int actorIndexInAct;
				ActorAct[] actualActorActs;
				int actIndex;
				for (int actionType = 0; actionType < acts.length / 2; actionType++) { // цикл
																						// по
																						// типам
																						// действий
					if (myActions.get(i).getActName().contains(ActionTypes.values()[actionType].toString())) {
						currentActor = 0;
						actIndex = actionType * 2;
						actualActorActs = new ActorAct[acts[actIndex]];
						for (int j = 0; j < teamMatesCount.get(i); j++) // цикл
																		// по
																		// партнерам
						{
							if (teamMatesCount.get(i) > 0) {
								combinationCounter = combinationCounterAtStart;
								int frequency = calculateFrequency(actIndex, i, currentActor);
								int period = actionCount[2 * i] * actionCount[2 * i + 1] / frequency;
								actorIndexInAct = currentActor;
								actualActorActs = generateActualActorActs(actIndex, teamMateInfoIn, currentActor,
										gateCoord);
								for (int m = 0; m < period; m++) {
									for (int k = 0; k < frequency; k++) {
										allocateSituation(i, combinationCounter);
										situations[combinationCounter].setAct(actorIndexInAct,
												actualActorActs[m % acts[actIndex]]);
										situations[combinationCounter].setActIndex(i);
										combinationCounter++;
									}
								}
								currentActor++;
							}
						}
						currentActor = 0;
						actIndex = actionType * 2 + 1;
						actualActorActs = new ActorAct[acts[actIndex]];
						for (int j = 0; j < enemyCount.get(i); j++) // цикл по
																	// оппонентам
						{
							if (enemyCount.get(i) > 0) {
								combinationCounter = combinationCounterAtStart;
								int frequency = calculateFrequency(actIndex, i, currentActor);
								int period = actionCount[2 * i] * actionCount[2 * i + 1] / frequency;
								actorIndexInAct = teamMatesCount.get(i) + currentActor;
								actualActorActs = generateActualActorActs(actIndex, enemyInfoIn, currentActor,
										gateCoord);
								for (int m = 0; m < period; m++) {
									for (int k = 0; k < frequency; k++) {
										allocateSituation(i, combinationCounter);
										situations[combinationCounter].setAct(actorIndexInAct,
												actualActorActs[m % acts[actIndex]]);
										situations[combinationCounter].setActIndex(i);
										combinationCounter++;
									}
								}
								currentActor++;
							}
						}
						combinationCounterAtStart = combinationCounter;
					}
				}
			}
		}
		return true;
	}
	/**
	 * Метод для проверки, сможет ли оппонент перехватить мяч при подаче мяча в определённом направлении и на определённое расстояние
	 * @param myAction действие планирующего игрока
	 * @param enemyDirection направление на оппонента
	 * @param enemyDistance расстояние до оппонента
	 * @param inputData входные данные
	 * @return да/нет
	 */
	private boolean opponentIsInWay(ActorAct myAction, double enemyDirection, double enemyDistance, InputData inputData)
	{
		double ballSpeedOnTact = ServerParameters.ball_speed_max;
		double ballPathDistance = 0.0;
		int actionLength = AbsoluteCoordinate.calculatePlannedActionLength(inputData.getMyCoord(), ballSpeedOnTact, myAction.getActParamsAbs());
		for (int tact = 1; tact <= actionLength; tact++) {
			ballPathDistance += ballSpeedOnTact;
			AbsoluteCoordinate myPassPathCoordinate = AbsoluteCoordinate.relativeToAbsoluteWithBase(inputData.getMyCoord(), inputData.getMyNeckPosition(),
					myAction.getActParams().getDirection(), ballPathDistance);
			AbsoluteCoordinate enemyCoordinate = AbsoluteCoordinate.relativeToAbsoluteWithBase(inputData.getMyCoord(), inputData.getMyNeckPosition(),
					enemyDirection, enemyDistance);
			ballSpeedOnTact = ballSpeedOnTact * Constants.BALL_DECAY;
			if (AbsoluteCoordinate.isCoordinateReachable(enemyCoordinate, myPassPathCoordinate, tact)) {
				return true;
			}
		}
		return false;
	}
}
