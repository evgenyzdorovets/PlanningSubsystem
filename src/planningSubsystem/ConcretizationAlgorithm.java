package planningSubsystem;

import java.util.ArrayList;

import planningSubsystem.Constants.ActionTypes;
import planningSubsystem.Constants.ActorType;
import planningSubsystem.Constants.OurTeamSide;

/**
 * Класс алгоритма конкретизации действий
 * @author Евгений Здоровец
 */
public class ConcretizationAlgorithm {
	private InputData previousInputData = null;
	private ArrayList<ArrayList<PlayerInfo>> actualTeamMateInfoArray = null;
	private ArrayList<ArrayList<PlayerInfo>> actualEnemyInfoArray = null;
	private ArrayList<ActorAct> myActions = null;
	/**
	 * Метод для задания актуальных игроков-партнёров
	 * @param actualTeamMateInfoArray массив актуальных игроков-партнёров
	 */
	public void setTeamMateInfo(ArrayList<ArrayList<PlayerInfo>> actualTeamMateInfoArray) {
		this.actualTeamMateInfoArray = actualTeamMateInfoArray;
	}
	/**
	 * Метод для задания актуальных игроков-оппонентов
	 * @param actualEnemyInfoArray массив актуальных игроков-оппонентов
	 */
	public void setEnemyInfo(ArrayList<ArrayList<PlayerInfo>> actualEnemyInfoArray) {
		this.actualEnemyInfoArray = actualEnemyInfoArray;
	}
	/**
	 * Метод для задания действий планирующего игрока
	 * @param myActions массив действий планирующего игрока
	 */
	public void setMyActions(ArrayList<ActorAct> myActions) {
		this.myActions = myActions;
	}
	
	/**
	 * Метод для конкретизации базовых действий. Перед вызовом данного метода должны быть заданы:
	 * массив актуальных игроков-партнёров, массив актуальных игроков-оппонентов, список действий планирующего игрока.
	 * @param inputData входные данные
	 * @param tactsLeft оставшееся количество тактов до завершения действия
	 * @return true
	 */
	public boolean concretizeActions(InputData inputData, int tactsLeft) {
		// Метод для конкретизации базовых действий
		int max_i = myActions.size();
		boolean[] deleteAction = new boolean[max_i];
		for (int i = 0; i < max_i; i++) {
			deleteAction[i] = false;
			int containsIndex = -1;
			for (int j = 0; j < Constants.sectoralDirections.length; j++)
				if (myActions.get(i).getActName().contains(Constants.sectoralDirections[j])) {
					containsIndex = j;
				}
			double baseAngle = 0.0;
			if (inputData.getOurSide() == OurTeamSide.LEFT && containsIndex != -1)
				baseAngle = Angle.mod180(Constants.sectoralAngles_FOR_LEFT_SIDE[containsIndex]);
			if (inputData.getOurSide() == OurTeamSide.RIGHT && containsIndex != -1)
				baseAngle = Angle.mod180(Constants.sectoralAngles_FOR_RIGHT_SIDE[containsIndex]);
			if (myActions.get(i).getActName().contains(ActionTypes.DRIBBLING.toString())) {
				myActions.get(i).setActionType(0);
					if (containsIndex != -1) {
						ArrayList<PlayerInfo> actualTeamMateInfo = actualTeamMateInfoArray.get(i);
						ArrayList<PlayerInfo> actualEnemyInfo = actualEnemyInfoArray.get(i);
						AbsoluteCoordinate myPredictedCoord;
						ArrayList<PlayerInfo> enemyPredictedInfo = new ArrayList<PlayerInfo>();
						if (previousInputData != null) {
							RelativeCoordinate myPrevCoord = new RelativeCoordinate(0.0, 0.0);
							RelativeCoordinate myCurrentCoord = RelativeCoordinate.absoluteToRelative(previousInputData.getMyCoord(), inputData.getMyCoord(), inputData.getMyNeckPosition());
							double direction = myCurrentCoord.getDirection();
							double distance = myCurrentCoord.getDistance();
							// предсказание местоположения планирующего игрока по относительным координатам
							myPredictedCoord = predictPlayerCoordinate(tactsLeft, inputData, direction, distance, myPrevCoord);
							// предсказание местоположения игроков-оппонентов
							predictEnemyPos(actualEnemyInfo, tactsLeft, inputData, myPredictedCoord, enemyPredictedInfo);
						}
						else {
							myPredictedCoord = inputData.getMyCoord();
							enemyPredictedInfo = actualEnemyInfo;
						}
						AbsoluteCoordinate closestIntersectionToMe = buildPerpendiculars(myPredictedCoord,
								inputData.getMyNeckPosition(), baseAngle, enemyPredictedInfo);
						if (closestIntersectionToMe != null) {
							ActorAct action = myActions.get(i);
							// задание параметров действия "ведение мяча" в виде относительной координаты
							action.setActParams(RelativeCoordinate.absoluteToRelative(inputData.getMyCoord(), closestIntersectionToMe, inputData.getMyNeckPosition()));
							// задание параметров действия "ведение мяча" в виде абсолютной координаты
							action.setActParamsAbs(closestIntersectionToMe);
						}
					}
			} else if (myActions.get(i).getActName().contains(ActionTypes.PASS.toString())) {
				myActions.get(i).setActionType(1);
				ArrayList<PlayerInfo> actualTeamMateInfo = actualTeamMateInfoArray.get(i);
				ArrayList<PlayerInfo> actualEnemyInfo = actualEnemyInfoArray.get(i);
				ActorAct existingAction = myActions.get(i);
				boolean actionWasConcretized = existingAction.isConcretized();
				if (!actionWasConcretized)
					if (actualTeamMateInfo.size() > 0)
						deleteAction[i] = true;
				for (int k = 0; k < actualTeamMateInfo.size(); k++) {
					ArrayList<PlayerInfo> enemyPredictedInfo = new ArrayList<PlayerInfo>();
					PlayerInfo teamMate = actualTeamMateInfo.get(k);
					double direction = teamMate.getRelativeCoordinates().getDirection();
					double distance = teamMate.getRelativeCoordinates().getDistance();
					direction = Math.floor(direction * 100) / 100;
					distance = Math.floor(distance * 100) / 100;
					ActorAct actionToChange;
					if (!actionWasConcretized)
						actionToChange = existingAction.clone();
					else {
						actionToChange = existingAction;
						int index = getIndexWithNumber(teamMate.getNumber());
						if (index != -1) {
							// предсказание местоположения игрока-адресата паса по относительным координатам
							AbsoluteCoordinate predictedTeammateCoord = predictPlayerCoordinate(tactsLeft, inputData, direction, distance, previousInputData.getTeamMateInfoIn().get(index).getRelativeCoordinates());
							// предсказание местоположения игроков-оппонентов
							predictEnemyPos(actualEnemyInfo, tactsLeft, inputData, predictedTeammateCoord, enemyPredictedInfo);
						}
					}
					if (!actionWasConcretized
							|| actionWasConcretized && actionToChange.getActorNumber() == teamMate.getNumber()) {
						AbsoluteCoordinate predictedTeamMateCoordinate = AbsoluteCoordinate.relativeToAbsoluteWithBase(inputData.getMyCoord(), inputData.getMyNeckPosition(), direction, distance);
						AbsoluteCoordinate closestIntersectionToPlayer = buildPerpendiculars(predictedTeamMateCoordinate,
								inputData.getMyNeckPosition(), baseAngle, enemyPredictedInfo);
						// задание параметров действия "пас" в виде относительной координаты
						actionToChange.setActParams(RelativeCoordinate.absoluteToRelative(inputData.getMyCoord(), closestIntersectionToPlayer, inputData.getMyNeckPosition()));
						// задание параметров действия "пас" в виде абсолютной координаты
						if(closestIntersectionToPlayer != null)
							actionToChange.setActParamsAbs(closestIntersectionToPlayer);
					}
					actionToChange.setActorNumber(teamMate.getNumber());
					if (!actionWasConcretized)
						myActions.add(actionToChange);
				}
			} else if (myActions.get(i).getActName().contains(ActionTypes.KICK_TO_GATE.toString())) {
				myActions.get(i).setActionType(2);
				RelativeCoordinate gateCoord = RelativeCoordinate.calculateGateRelativeCoordinate(inputData);
				// задание параметров действия "удар по воротам" в виде относительной координаты
				myActions.get(i).setActParams(gateCoord);
				AbsoluteCoordinate actionAbsoluteCoordinate = AbsoluteCoordinate.relativeToAbsoluteWithBase(
						inputData.getMyCoord(), inputData.getMyNeckPosition(), gateCoord.getDirection(), gateCoord.getDistance());
				// задание параметров действия "удар по воротам" в виде абсолютной координаты
				myActions.get(i).setActParamsAbs(actionAbsoluteCoordinate);
			}
		}
		for (int i = 0; i < max_i; i++) {
			if (deleteAction[i]) {
				myActions.remove(i);
				for (int j = i + 1; j < max_i; j++) {
					if (deleteAction[j]) {
						deleteAction[j - 1] = true;
						deleteAction[j] = false;
					}
				}
			}
		}
		int size;
		boolean deleted = true;
		while (deleted) {
			size = myActions.size();
			deleted = false;
			for (int i = 0; i < size; i++) {
				for (int j = i + 1; j < size; j++) {
					if (i != j) {
						if (myActions.get(i).getActName() == myActions.get(j).getActName())
							if (myActions.get(i).getActorNumber() == myActions.get(j).getActorNumber()) {
								deleted = true;
								myActions.remove(j);
								size = myActions.size();
							}
					}
				}
			}
		}
		previousInputData = new InputData(inputData.getTeamMateInfoIn(), inputData.getEnemyInfoIn(), inputData.getMyCoord(), inputData.getMyNeckPosition());
		return true;
	}
	/**
	 * Метод для построения метода перпендикуляров
	 * @param playerCoord координата игрока, для которого конкретизируется действие
	 * @param playerNeckPosition угол поворота игрока 
	 * @param baseAngle базовый угол направления действия
	 * @param actualEnemyInfo информация об актуальных игроках-оппонентах
	 * @return абсолютная координата для проброса мяча
	 */
	private AbsoluteCoordinate buildPerpendiculars(AbsoluteCoordinate playerCoord, double playerNeckPosition,
			double baseAngle, ArrayList<PlayerInfo> actualEnemyInfo) {
		ArrayList<Line> perpendicularLines = new ArrayList<Line>();
		AbsoluteCoordinate enemyHalfWayCoordinate = null;
		for (int enemy = 0; enemy < actualEnemyInfo.size(); enemy++) {
			double enemyDirection = actualEnemyInfo.get(enemy).getRelativeCoordinates().getDirection();
			double enemyDistance = actualEnemyInfo.get(enemy).getRelativeCoordinates().getDistance();
			// вычисление абсолютной серединной координаты между
			// планирующим игроком и оппонентом
			// enemy (с запасом в один такт)
			enemyHalfWayCoordinate = AbsoluteCoordinate.relativeToAbsoluteWithBase(playerCoord, playerNeckPosition,
					enemyDirection, enemyDistance / 2 - Constants.PLAYER_SPEED_PER_TACT);
			// вычисление коэффициента k прямой до противника,
			// проходящей через координату планирующего игрока и
			// вычисленную ранее серединную координату между
			// планирующим игроком и оппонентом enemy
			double lineToEnemyK = (enemyHalfWayCoordinate.getY() - playerCoord.getY()) / (enemyHalfWayCoordinate.getX() - playerCoord.getX());
			// вычисление коэффициента b прямой до противника,
			// проходящей через координату планирующего игрока и
			// вычисленную ранее серединную координату между
			// планирующим игроком и оппонентом enemy
			double lineToEnemyB = (playerCoord.getY() - enemyHalfWayCoordinate.getY()) * playerCoord.getX()
					/ (enemyHalfWayCoordinate.getX() - playerCoord.getX()) + playerCoord.getY();
			// вычисление коэффициента k прямой, проходящей
			// через серединную координату и перпендикулярной
			// прямой до противника
			double linePerpendicularToEnemyK = -lineToEnemyK;
			// вычисление коэффициента b прямой, проходящей
			// через серединную координату и перпендикулярной
			// прямой до противника
			double linePerpendicularToEnemyB = enemyHalfWayCoordinate.getY()
					- linePerpendicularToEnemyK * enemyHalfWayCoordinate.getX();
			perpendicularLines.add(new Line(linePerpendicularToEnemyK, linePerpendicularToEnemyB));
		}
		int lineCount = perpendicularLines.size();
		double intersectionX, intersectionY;
		double minDistance = 999;
		AbsoluteCoordinate closestIntersectionToPlayer = null;
		if (lineCount == 0) {
			closestIntersectionToPlayer = AbsoluteCoordinate.relativeToAbsoluteWithBase(playerCoord, playerNeckPosition, baseAngle,
					Constants.MAX_DASH_DISTANCE);
		} else if (lineCount == 1) {
			closestIntersectionToPlayer = enemyHalfWayCoordinate;
		}
		for (int line1 = 0; line1 < lineCount; line1++) {
			for (int line2 = line1 + 1; line2 < lineCount; line2++) {
				intersectionX = (perpendicularLines.get(line2).b - perpendicularLines.get(line1).b)
						/ (perpendicularLines.get(line1).k - perpendicularLines.get(line2).k);
				intersectionY = perpendicularLines.get(line1).k * intersectionX + perpendicularLines.get(line1).b;
				AbsoluteCoordinate intersectionCoordinate = new AbsoluteCoordinate(intersectionX, intersectionY);
				double distanceToMe = AbsoluteCoordinate.getDistanceBetweenCoords(playerCoord,
						intersectionCoordinate);
				if (minDistance > distanceToMe 
						&& Math.abs(intersectionCoordinate.getX()) <= Constants.MAX_X 
						&& Math.abs(intersectionCoordinate.getY()) <= Constants.MAX_Y) {
					minDistance = distanceToMe;
					closestIntersectionToPlayer = intersectionCoordinate;
				}
			}
		}
		return closestIntersectionToPlayer;
	}
	/**
	 * Метод для определения индекса игрока-партнёра с заданным номером в массиве previousInputData
	 * @param number номер игрока-партнёра
	 * @return индекс в массиве inputDataCopy
	 */
	private int getIndexWithNumber(int number) {
		for (int i = 0; i < previousInputData.getTeamMateInfoIn().size(); i++) {
			if (previousInputData.getTeamMateInfoIn().get(i).getNumber() == number)
				return i;
		}
		return -1;
	}
	/**
	 * Метод для предсказания местоположения игрока по относительным координатам
	 * @param tactsLeft количество оставшихся тактов до завершения текущего действия
	 * @param inputData входные данные
	 * @param direction направление на игрока (в текущем такте)
	 * @param distance расстояние до игрока (в текущем такте)
	 * @param prevCoord относительная координата на игрока (в предыдущем такте)
	 * @return абсолютная координата игрока
	 */
	private AbsoluteCoordinate predictPlayerCoordinate(int tactsLeft, InputData inputData, double direction, double distance, RelativeCoordinate prevCoord) {
		RelativeCoordinate coordinateChange = new RelativeCoordinate();
		coordinateChange.setDirection(Angle.mod180(direction - prevCoord.getDirection()));
		coordinateChange.setDistance(distance - prevCoord.getDistance());
		direction = Angle.mod180(direction + tactsLeft * coordinateChange.getDirection());
		distance += tactsLeft * coordinateChange.getDistance();
		AbsoluteCoordinate predictedCoord = AbsoluteCoordinate.relativeToAbsoluteWithBase(inputData.getMyCoord(), inputData.getMyNeckPosition(), direction, distance);
		return predictedCoord;
	}
	/**
	 * Метод для предсказания относительных координат оппонентов
	 * @param actualEnemyInfo информация об игроках-оппонентах
	 * @param tactsLeft количество оставшихся тактов до завершения текущего действия
	 * @param inputData входные данные
	 * @param playerCoord предсказанная координата игрока, относительно которого вычисляются позиции
	 * @param enemyPredictedInfo пустой массив предсказанной информации об оппонентах
	 */
	private void predictEnemyPos(ArrayList<PlayerInfo> actualEnemyInfo, int tactsLeft, InputData inputData, AbsoluteCoordinate playerCoord, ArrayList<PlayerInfo> enemyPredictedInfo) {
		for (int en = 0; en < actualEnemyInfo.size(); en++) {
			PlayerInfo enemy = actualEnemyInfo.get(en);
			double enemyDirection = enemy.getRelativeCoordinates().getDirection();
			double enemyDistance = enemy.getRelativeCoordinates().getDistance();
			RelativeCoordinate enemyCoordinateChange = new RelativeCoordinate();
			enemyCoordinateChange.setDirection(Angle.mod180(enemyDirection
					- previousInputData.getEnemyInfoIn().get(en).getRelativeCoordinates().getDirection()));
			enemyCoordinateChange.setDistance(enemyDistance
					- previousInputData.getEnemyInfoIn().get(en).getRelativeCoordinates().getDistance());
			enemyDirection = Angle.mod180(enemyDirection + tactsLeft * enemyCoordinateChange.getDirection());
			enemyDistance += tactsLeft * enemyCoordinateChange.getDistance();
			AbsoluteCoordinate predictedEnemyCoord = AbsoluteCoordinate.relativeToAbsoluteWithBase(
					inputData.getMyCoord(), inputData.getMyNeckPosition(), enemyDirection, enemyDistance);
			// предсказанное направление на игрока-оппонента по отношению к игроку-адресату паса
			double predictedDirection = Angle.mod180(AbsoluteCoordinate.getDirectionOfCoordinate(playerCoord, predictedEnemyCoord) - inputData.getMyNeckPosition());
			// предсказанное расстояние до игрока-оппонента от игрока-адресата паса
			double predictedDistance = AbsoluteCoordinate.getDistanceBetweenCoords(predictedEnemyCoord, playerCoord);
			enemyPredictedInfo.add(new PlayerInfo(predictedDirection, predictedDistance, enemy.getNumber(), ActorType.ENEMY));
		}
	}
}
