package planningSubsystem;

/**
 * Класс ситуации
 * @author Евгений Здоровец
 */
public class Situation {
	private int actIndex; // индекс действия планирующего игрока в списке базовых действий
	private ActorAct[] acts; // действия актуальных акторов в данной ситуации
	private int actLength;
	private double utility = 0.0;
	/**
	 * Метод для получения индекса действия планирующего игрока в списке базовых действий
	 * @return индекс действия планирующего игрока в списке базовых действий
	 */
	public int getActIndex(){
		return actIndex;
	}
	/**
	 * Метод для задания индекса действия
	 * @param value индекс действия для присваивания
	 */
	public void setActIndex(int value){
		actIndex=value;
	}
	/**
	 * Метод для получения действия с указанным индексом
	 * @param index индекс действия
	 * @return действие с указанным индексом
	 */
	public ActorAct getAct(int index){
		return acts[index];
	}
	/**
	 * Метод для задания действия по индексу
	 * @param index индекс действия
	 * @param act действие для присваивания
	 */
	public void setAct(int index, ActorAct act){
		acts[index] = act;
	}
	/**
	 * Метод для создания заданного количества действий
	 * @param count заданное количества действий
	 */
	public void allocateActs(int count){
		acts = new ActorAct[count];
		actLength = count;
	}
	/**
	 * Метод для получения количества актуальных акторов для данного действия
	 * @return количество актуальных акторов для данного действия
	 */
	public int getActorCount(){
		return actLength;
	}
	/**
	 * Метод для получения полезности прогнозируемой ситуации
	 * @return полезность прогнозируемой ситуации
	 */
	public double getPredictedSituationUtility(){
		return utility;
	}
	/**
	 * Метод для задания полезности прогнозируемой ситуации
	 * @param value полезность прогнозируемой ситуации для присваивания
	 */
	public void setPredictedSituationUtility(double value){
		utility = value;
	}
}
