package planningSubsystem;
/**
 * Класс констант
 * @author Евгений Здоровец
 */
public final class Constants { 
	public enum ActionTypes {
		DRIBBLING("Ведение"), 
		PASS("пас"),
		KICK_TO_GATE("удар по воротам");
		
		private String description;

		ActionTypes(String description) {
	        this.description = description;
	    }

	    @Override
	    public String toString() {
	        return description;
	    }
		};
	static String[] sectoralDirections = {"вперед", "влево", "вправо", "назад"};
	static Double[] sectoralAngles_FOR_LEFT_SIDE = {90.0, 0.0, 180.0, -90.0};
	static Double[] sectoralAngles_FOR_RIGHT_SIDE = {-90.0, 180.0, 0.0, 90.0};
	static double MAX_X = 104.0;
	static double MAX_Y = 64.0;
	static AbsoluteCoordinate GATE_LEFT = new AbsoluteCoordinate(0.0, MAX_Y/2);
	static AbsoluteCoordinate GATE_RIGHT = new AbsoluteCoordinate(MAX_X, MAX_Y/2);
	public enum OurTeamSide { LEFT, RIGHT};
	public enum ActorType { TEAMMATE, ENEMY, ME, UNINDENTIFIED};
	public static double PLAYER_SPEED_PER_TACT = 1.0; // 1.0 м за 100 мс
	public static double BALL_DECAY = 0.94;
	static double MAX_DASH_DISTANCE = 10.0;
	public static int getActorMultiplier(ActorType actorType) {
		if(actorType == ActorType.TEAMMATE)
			return 1;
		else if(actorType == ActorType.ENEMY)
			return -1;
		return 0;
	}
}