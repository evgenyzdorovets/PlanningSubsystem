; список правил для использования в случае выбора машины логического вывода

;; определение шаблона
(deftemplate signs
 (slot has_the_ball (type NUMBER)(default 0))
 (slot scheme (type STRING)) ; схема расстановки
 (slot position (type STRING)) ; позиция текущего игрока
 (slot player_with_the_ball_position (type STRING)) ; позиция игрока с мячом
)

(deftemplate result
 (slot act1 (type STRING)(default "Нет"))
 (slot act2 (type STRING)(default "Нет"))
 (slot act3 (type STRING)(default "Нет"))
)

;;;;;;;;;;;;;;;5-2-3, мяч не у игрока;;;;;;;;;;;;;;;;;
(defrule NotWithTheBall1
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "5-2-3") (position "ЛН") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЦН") (eq ?pos "ЛПЗ")) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall2
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "5-2-3") (position "ЦН") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛН") (or (eq ?pos "ПН") (or (eq ?pos "ЛПЗ") (eq ?pos "ППЗ")))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall3
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "5-2-3") (position "ПН") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЦН") (eq ?pos "ППЗ")) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall4
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "5-2-3") (position "ЛПЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛН") (or (eq ?pos "ЦН") (or (eq ?pos "ППЗ") (or (eq ?pos "ЛЗ") (or (eq ?pos "ЛЦЗ") (eq ?pos "ЦЗ")))))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall5
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "5-2-3") (position "ППЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ПН") (or (eq ?pos "ЦН") (or (eq ?pos "ЛПЗ") (or (eq ?pos "ПЗ") (or (eq ?pos "ПЦЗ") (eq ?pos "ЦЗ")))))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall6
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "5-2-3") (position "ЛЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛПЗ") (eq ?pos "ЛЦЗ")) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall7
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "5-2-3") (position "ЛЦЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛПЗ") (or (eq ?pos "ППЗ") (eq ?pos "ЦЗ"))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall8
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "5-2-3") (position "ЦЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛПЗ") (or (eq ?pos "ППЗ") (or (eq ?pos "ЛЦЗ") (eq ?pos "ПЦЗ")))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall9
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "5-2-3") (position "ПЦЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ППЗ") (or (eq ?pos "ЦЗ") (eq ?pos "ПЗ"))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall10
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "5-2-3") (position "ПЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ППЗ") (eq ?pos "ПЦЗ")) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

;;;;;;;;;;;;;;;4-3-3, мяч не у игрока;;;;;;;;;;;;;;;;;
(defrule NotWithTheBall11
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-3-3") (position "ЛН") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЦН") (or (eq ?pos "ЛПЗ") (eq ?pos "ЦПЗ"))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall12
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-3-3") (position "ЦН") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛН") (or (eq ?pos "ПН") (or (eq ?pos "ЛПЗ") (or (eq ?pos "ЦПЗ") (eq ?pos "ППЗ"))))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall13
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-3-3") (position "ПН") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЦН") (or (eq ?pos "ППЗ") (eq ?pos "ЦПЗ"))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall14
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-3-3") (position "ЛПЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛН") (or (eq ?pos "ЦН") (or (eq ?pos "ЦПЗ") (or (eq ?pos "ЛЗ") (eq ?pos "ЛЦЗ"))))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall15
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-3-3") (position "ЦПЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛН") (or (eq ?pos "ЦН") (or (eq ?pos "ПН") (or (eq ?pos "ЛПЗ") (or (eq ?pos "ППЗ") (or (eq ?pos "ЛЦЗ") (eq ?pos "ПЦЗ"))))))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall16
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-3-3") (position "ППЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ПН") (or (eq ?pos "ЦН") (or (eq ?pos "ЦПЗ") (or (eq ?pos "ПЗ") (eq ?pos "ПЦЗ"))))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall17
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-3-3") (position "ЛЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛПЗ") (eq ?pos "ЛЦЗ")) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall18
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-3-3") (position "ЛЦЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛПЗ") (or (eq ?pos "ЦПЗ") (or (eq ?pos "ЛЗ") (eq ?pos "ПЦЗ")))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall19
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-3-3") (position "ПЦЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ППЗ") (or (eq ?pos "ЦПЗ") (or (eq ?pos "ПЗ") (eq ?pos "ЛЦЗ")))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall20
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-3-3") (position "ПЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ППЗ") (eq ?pos "ПЦЗ")) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall21
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-4-2") (position "ЛН") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ПН") (or (eq ?pos "ЛПЗ") (or (eq ?pos "ЛЦПЗ") (eq ?pos "ПЦПЗ")))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall22
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-4-2") (position "ПН") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛН") (or (eq ?pos "ППЗ") (or (eq ?pos "ЛЦПЗ") (eq ?pos "ПЦПЗ")))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall23
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-4-2") (position "ЛПЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛН") (or (eq ?pos "ЛЦПЗ") (or (eq ?pos "ЛЦЗ") (eq ?pos "ЛЗ")))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall24
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-4-2") (position "ЛЦПЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛН") (or (eq ?pos "ПН") (or (eq ?pos "ПЦПЗ") (or (eq ?pos "ПЦЗ") (or (eq ?pos "ЛЦЗ") (or (eq ?pos "ЛЗ") (eq ?pos "ЛПЗ"))))))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall25
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-4-2") (position "ПЦПЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛН") (or (eq ?pos "ПН") (or (eq ?pos "ЛЦПЗ") (or (eq ?pos "ПЦЗ") (or (eq ?pos "ЛЦЗ") (or (eq ?pos "ПЗ") (eq ?pos "ППЗ"))))))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall26
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-4-2") (position "ППЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ПН") (or (eq ?pos "ПЦПЗ") (or (eq ?pos "ПЦЗ") (eq ?pos "ПЗ")))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall27
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-4-2") (position "ЛЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛПЗ") (or (eq ?pos "ЛЦПЗ") (eq ?pos "ЛЦЗ"))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall28
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-4-2") (position "ЛЦЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ЛЗ") (or (eq ?pos "ЛПЗ") (or (eq ?pos "ЛЦПЗ") (or (eq ?pos "ПЦПЗ") (eq ?pos "ПЦЗ"))))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall29
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-4-2") (position "ПЦЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ПЗ") (or (eq ?pos "ППЗ") (or (eq ?pos "ЛЦПЗ") (or (eq ?pos "ПЦПЗ") (eq ?pos "ЛЦЗ"))))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

(defrule NotWithTheBall30
(declare (salience 1000))
(signs (has_the_ball 0) (scheme "4-4-2") (position "ПЗ") (player_with_the_ball_position ?pos))
(test (or (eq ?pos "ППЗ") (or (eq ?pos "ПЦПЗ") (eq ?pos "ПЦЗ"))) )
=>
(assert (result (act1 "Открыться для приема паса") (act2 "Закрыть зону") (act3 "Нет"))) )

;;;;;;;;на случай, если мяч был не у смежного игрока;;;;;;;;
(defrule Check_assertion
(declare (salience 900))
(signs (has_the_ball 0))
(not (result (act1 ?act)))
=>
(assert (result (act1 "Закрыть зону") (act2 "Нет") (act3 "Нет")))
)

;;;;;;;;;;;;;;;5-2-3, мяч у игрока;;;;;;;;;;;;;;;;;
(defrule HasTheBall1
(declare (salience 500))
(signs (has_the_ball 1) (scheme "5-2-3") (position "ЛН"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall2
(declare (salience 500))
(signs (has_the_ball 1) (scheme "5-2-3") (position "ЦН"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, влево, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall3
(declare (salience 500))
(signs (has_the_ball 1) (scheme "5-2-3") (position "ПН"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (влево, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall4
(declare (salience 500))
(signs (has_the_ball 1) (scheme "5-2-3") (position "ЛПЗ"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, вперед, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall5
(declare (salience 500))
(signs (has_the_ball 1) (scheme "5-2-3") (position "ППЗ"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (влево, вперед, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall6
(declare (salience 500))
(signs (has_the_ball 1) (scheme "5-2-3") (position "ЛЗ"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, вперед, назад)"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall789
(declare (salience 500))
(signs (has_the_ball 1) (scheme "5-2-3") (position ?pos))
(test (or (eq ?pos "ЛЦЗ") (or (eq ?pos "ЦЗ") (eq ?pos "ПЦЗ"))))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, влево, вперед, назад)"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall10
(declare (salience 500))
(signs (has_the_ball 1) (scheme "5-2-3") (position "ПЗ"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (влево, вперед, назад)"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;4-3-3, мяч у игрока;;;;;;;;;;;;;;;;;
(defrule HasTheBall11
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-3-3") (position "ЛН"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall12
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-3-3") (position "ЦН"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, влево, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall13
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-3-3") (position "ПН"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (влево, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall14
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-3-3") (position "ЛПЗ"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, вперед, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall15
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-3-3") (position "ППЗ"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (влево, вперед, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall16
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-3-3") (position "ЦПЗ"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, влево, вперед, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall17
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-3-3") (position "ЛЗ"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, вперед, назад)"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall1819
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-3-3") (position ?pos))
(test (or (eq ?pos "ЛЦЗ") (eq ?pos "ПЦЗ")))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, влево, вперед, назад)"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall20
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-3-3") (position "ПЗ"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (влево, вперед, назад)"))) )

;;;;;;;;;;;;;;;4-4-2, мяч у игрока;;;;;;;;;;;;;;;;;
(defrule HasTheBall21
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-4-2") (position "ЛН"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall22
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-4-2") (position "ПН"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (влево, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall23
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-4-2") (position "ЛПЗ"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, вперед, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall2425
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-4-2") (position ?pos))
(test (or (eq ?pos "ЛЦПЗ") (eq ?pos "ПЦПЗ")))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, влево, вперед, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall26
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-4-2") (position "ППЗ"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (влево, вперед, назад)") (act3 "удар по воротам"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall27
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-4-2") (position "ЛЗ"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, вперед, назад)"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall2829
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-4-2") (position ?pos))
(test (or (eq ?pos "ЛЦЗ") (eq ?pos "ПЦЗ")))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (вправо, влево, вперед, назад)"))) )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule HasTheBall30
(declare (salience 500))
(signs (has_the_ball 1) (scheme "4-4-2") (position "ПЗ"))
=>
(assert (result (act1 "Ведение (вперед, влево, вправо, назад)") (act2 "пас (влево, вперед, назад)"))) )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defrule Check_result
(declare (salience 100))
(not (result (act1 ?act)))
=>
(printout t "Неправильно заданы входные параметры." crlf)
)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;